# AyD2_Proyecto1_Grupo17
# Comando utilizados para levantar los contendores del Servicio3
Actualmente obsoltes, se utiliza docker-compose para desplegar todos los contenedores.

======================== MICROSERVICIOS PARA OBTENER ORDENES ========================  
docker build -t msobtener:1.0 .  
docker run -dit -p 3010:3010 --name ms1 msobtener:1.0  

======================== MICROSERVICIOS PARA ELIMINAR ORDENES ========================   
docker build -t mseliminar:1.0 .  
docker run -dit -p 3011:3011 --name ms2 mseliminar:1.0  

======================== MICROSERVICIOS PARA CREAR ORDENES ========================  
docker build -t mscrear:1.0 .  
docker run -dit -p 3012:3012 --name ms3 mscrear:1.0  

======================== MICROSERVICIOS PARA ACTUALIZAR ORDENES ========================  
docker build -t msactualizar:1.0 .  
docker run -dit -p 3013:3013 --name ms4 msactualizar:1.0  
#!/bin/bash
set -eo pipefail
echo "$CI_COMMIT_BRANCH"

echo "Subiendo las imagenes de los contenedores de prueba a DockerHub"

# Se agregan los tag a las imagenes, puede construirse nuevamente con... docker build -t
# docker build -t ajme/ajme-prog/ayd2_proyecto1_grupo17 .

echo "login"
sudo docker login -u ajme -p 199715alan
echo "Tagging"
docker tag frontend ajme/ajme-prog/ayd2_proyecto1_grupo17:frontend${VERSION}
docker tag ms-orden-eliminar ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-eliminar${VERSION}
docker tag ms-orden-obtener ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-obtener${VERSION}
docker tag ms-orden-actualizar ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-actualizar${VERSION}
docker tag servicio2 ajme/ajme-prog/ayd2_proyecto1_grupo17:servicio2${VERSION}
docker tag servicio1 ajme/ajme-prog/ayd2_proyecto1_grupo17:servicio1${VERSION}
docker tag ms-orden-crear ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-crear${VERSION}
docker tag mydb ajme/ajme-prog/ayd2_proyecto1_grupo17:mydb${VERSION}

echo "Pushing"
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:frontend${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-eliminar${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-obtener${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-actualizar${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:servicio2${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:servicio1${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-crear${VERSION}
docker push ajme/ajme-prog/ayd2_proyecto1_grupo17:mydb${VERSION}

echo "End of script"

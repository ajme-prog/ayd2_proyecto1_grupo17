const mysql = require('mysql');

const pool = mysql.createPool({
   connectionLimit : 10,
   host            : process.env.HOST_DB,
   user            : process.env.USER_DB,
   password        : process.env.PASSWORD_DB,
   database        : process.env.DATABASE,
   port            : process.env.PORT_DB
});

let connection = {};

connection.query = (sqlString, values = []) => {
  return new Promise((resolve, reject) => {
    pool.query(sqlString, values,function (error, results /*, fields*/) {
      if (error) return reject(error);
      return resolve(results)
    });
  });
}

module.exports = connection;
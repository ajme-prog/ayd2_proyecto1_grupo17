const {nombreServidor} = require('./data');

async function insertErrorSistema(connection, parameters) {

    const query = `INSERT INTO Error(fecha,nivelError,origen,mensajeErrorSistema) 
                   VALUES(NOW(),'Sistema','${nombreServidor.nombre}',?)`;

    connection.query(query, parameters , (err, rows)=>{
        if(err) throw err;

        console.log(rows);
    });
}

async function insertErrorUsuario(connection, parameters) {

    const query = `INSERT INTO Error(fecha,nivelError,origen,descripcionError,entradaError,Usuario_idUsuario) 
                   VALUES(NOW(),'Usuario','${nombreServidor.nombre}',?,?,?)`;

    connection.query(query, parameters , (err, rows)=>{
        if(err) throw err;

        console.log(rows);
    });
}

module.exports = {
    insertErrorSistema,
    insertErrorUsuario
};
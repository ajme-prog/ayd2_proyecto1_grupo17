const connection = require('../services/dbmysql');
const {
    insertErrorSistema,
    insertErrorUsuario
} = require('../services/log');
const {
    create_orderDB,
    create_detalle_orderDB
} = require("../db_apis/ordersDB");

async function createOrder(req,res) {
    const id_usuario= parseInt(req.body.id_usuario);
    const tipo_orden= req.body.tipo_orden;
    const estado_orden= req.body.estado_orden;
    const id_direccion = req.body.id_direccion;
    const id_restaurante = req.body.id_restaurante;
    const lista_productos = req.body.productos;

    let idNewOrder = 0;
    // INSERTAR LA ORDEN EN LA TABLA ORDEN
    try {
        idNewOrder = await create_orderDB(tipo_orden,estado_orden,id_usuario,id_direccion,id_restaurante,lista_productos.length);
        //res.status(200);
        //return res.json(r)
    }catch(err) {
        console.log( err)
        insertErrorSistema(connection, JSON.stringify(err) );
        return res.status(404).send({ status:404, message: "Error no se pudo registrar la orden" });
    }

    // INSERTAR TODO EL DETALLE DE LA ORDEN
    try {
        await create_detalle_orderDB(idNewOrder, lista_productos);
        return res.status(200).send({status:200,message:"Orden Agregada correctamente"});
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err2) );
        return res.status(404).send({ status:404, message: "Error no se pudo registrar los productos" })
    }
}

module.exports = {
    createOrder
}

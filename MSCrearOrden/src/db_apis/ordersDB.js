const connection = require('../services/dbmysql');

const {nombreServidor} = require('../services/data');

async function create_orderDB(tipo_orden,estado_orden,id_usuario,id_direccion,id_restaurante,contProductos) {
    // DATOS PARA EL LOG
    const origen = nombreServidor.nombre;
    const descripcion = `Se creo una nueva orden de tipo ${tipo_orden}, con un estado inicial ${estado_orden} y con ${contProductos} productos`;

    const query = 'SELECT insertOrden(?,?,?,?,?,?,?)';
    /*let lista_parametros = [];
    lista_parametros.push(id_usuario);
    lista_parametros.push(tipo_orden);
    lista_parametros.push(estado_orden);
    lista_parametros.push(origen);
    lista_parametros.push(descripcion);*/

    const rows = await( connection.query(query, [id_usuario,tipo_orden,estado_orden,id_direccion,id_restaurante,origen,descripcion]) );
    let key = Object.keys(rows[0]);
    let idOrden = rows[0][key[0]];

    return idOrden;
}

async function create_detalle_orderDB(idOrden, lista_productos) {
    for(let i=0;i<lista_productos.length;i++){
        let cantidad=lista_productos[i].cantidad;
        let precio = lista_productos[i].precio;
        let id_producto= lista_productos[i].id_producto;

        const query = `INSERT INTO detalleOrdenProducto(Orden_idOrden,Producto_idProducto,cantidadProducto,precio)
                        VALUES (?,?,?,?)`;

        await( connection.query(query, [idOrden,id_producto,cantidad,precio] ) );
    }
}


module.exports = {
    create_orderDB,
    create_detalle_orderDB
}
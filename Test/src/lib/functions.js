let Page = require('./driver');

let searchInput,searchInput1,searchInput2, searchButton, resultStat;


Page.prototype.findInputAndButton = async function(input, btn) {
    searchInput = await this.findByName(input);
    searchButton = await this.findByName(btn);

    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await searchInput.isEnabled();
        const searchButtonText = await searchButton.getAttribute('value');
        return {
            inputEnabled: searchInputEnableFlag,
            buttonText: searchButtonText
        }
    }, 5000);
    return result;
};

Page.prototype.findInputAuthenticationClickBefore = async function(input1, input2,btn) {
    await this.click(btn);
    searchInput1 = await this.findById(input1);
    searchInput2 = await this.findById(input2);

    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag1 = await searchInput1.isEnabled();
        const searchInputEnableFlag2 = await searchInput2.isEnabled();
        return {
            inputEnabled1: searchInputEnableFlag1,
            inputEnabled2: searchInputEnableFlag2
        }
    }, 5000);
    return result;
};

Page.prototype.authenticationTrue = async function(input1, input2,btn1,btn2,text1,text2) {
   await this.findInputAuthenticationClickBefore (input1, input2,btn1);
   await this.write(searchInput1, text1);
   await this.write(searchInput2, text2);
   await this.click(btn2);
   
   resultUsr = await this.getLocalStorage();
    return await this.driver.wait(async function() {
        return await resultUsr;
    },5000);

};

Page.prototype.findButtonById = async function(btn) {
    searchButton = await this.findById(btn);
    const result = await this.driver.wait(async function() {
        const searchButtonText = await searchButton.getAttribute('type');
        return {
            buttonText: searchButtonText
        }
    }, 5000);
    return result;
};

Page.prototype.findInputById = async function(input) {
    searchInput = await this.findById(input);
    
    const result = await this.driver.wait(async function() {
        const searchInputEnableFlag = await searchInput.isEnabled();
        return {
            inputEnabled: searchInputEnableFlag
        }
    }, 5000);
    return result;
};


Page.prototype.submitKeywordAndGetResult = async function(text) {
    await this.findInputAndButton('q','btnK');
    await this.write(searchInput, text);
    await searchInput.sendKeys("\n");

    resultStat = await this.findById("result-stats");
    return await this.driver.wait(async function() {
        return await resultStat.getText();
    },5000);
};

module.exports = Page;
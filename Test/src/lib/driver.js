const { utils } = require('mocha');
const { Builder, By, until, Key, JavascriptExecutor } = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;

const cbtHub = `http://${process.env.HUB_HOST}:4444/wd/hub`
//const cbtHub = `http://18.188.192.198:4444/wd/hub`
// const cbtHub = `http://localhost:4444/wd/hub`

const caps = {
    name: 'Chrome Test',
    setPageLoadStrategy: 'eager',
    browserName: 'chrome',
    browserVersion: '89.0.4389.82'
};

let Page = function() {

    this.driver = new Builder()
    //.setChromeOptions(options)
    .withCapabilities(caps)
    .usingServer(cbtHub)
    //.forBrowser('chrome')
    .build();


    // visit a webpage
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };


    // quit current session
    this.quit = async function() {
        return await this.driver.quit();
    };


    // wait and find a specific element with it's id
    this.findById = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return await this.driver.findElement(By.id(id));
       this.dr
    };


    // wait and find a specific element with it's name
    this.findByName = async function(name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
        return await this.driver.findElement(By.name(name));
    };

    // wait and find a specific element with it's name
    this.findByClass = async function(name) {
        await this.driver.wait(until.elementLocated(By.className(name)), 15000, 'Looking for element');
        return await this.driver.findElement(By.className(name));
    };

    // click a button
    this.click = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return await this.driver.findElement(By.id(id)).click();
    };

    // get local storage
    this.getLocalStorage = async function() {
        return await this.driver.executeScript("return localStorage.getItem('usr')");
    };

    // fill input web elements
    this.write = async function(el, txt) {
        return await el.sendKeys(txt);
    };

};

module.exports = Page;


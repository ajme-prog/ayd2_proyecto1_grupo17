let chai = require('chai');
const expect = require('chai').expect;

let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const url= `http://35.222.169.75:3030`;

describe('orders API', function() {
    before(function() {
      // runs once before the first test in this block
    });
  
    after(function() {
      // runs once after the last test in this block
    });
  
    beforeEach(function() {
      // runs before each test in this block
    });
  
    afterEach(function() {
      // runs after each test in this block
    });
  
    // =============  PRUEBA PARA OBTENER TODAS LAS ORDENES -- EXITOSA!!
    it('->MSObtenerOrden: Deberia obtener las ordenes exitosamente', (done) => {
      chai.request(url)
        .get('/orders/obtener-ordenes')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(200);
          done();
        });
    });


    // =============  PRUEBA PARA OBTENER ORDENES PENDIENTES -- EXITOSA!!
    it('->MSObtenerOrden: Deberia obtener las ordenes pendientes exitosamente', (done) => {
      chai.request(url)
        .get('/orders/ordenes-pendientes')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(200);
          done();
        });
    });


    // =============  PRUEBA PARA OBTENER ORDENES RESTAURANTE -- EXITOSA!!
    it('->MSObtenerOrden: Deberia obtener las ordenes por restaurante', (done) => {
      chai.request(url)
        .get('/orders/ordenes-restaurante?id_restaurante=1')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(200);
          done();
        });
    });

    // =============  PRUEBA PARA OBTENER ORDENES POR RESTURANTE -- FALLA!!
    it('->MSObtenerOrden: Deberia fallar obtener ordenes por restaurante, id no es numerico', (done) => {
      chai.request(url)
        .get('/orders/ordenes-restaurante?id_restaurante=noID')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(404);
          done();
        });
    });




    // =============  PRUEBA PARA OBTENER ORDENES POR USUARIO -- CORRECTA!!
    it('->MSObtenerOrden: Deberia obtener ordenes por usuario', (done) => {
      chai.request(url)
        .get('/orders/obtener-ordenes/3')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(200);
          done();
        });
    });


    // =============  PRUEBA PARA OBTENER ORDENES POR USUARIO -- FALLA!!
    it('->MSObtenerOrden: Deberia fallar obtener ordenes por usuario, no existe usuario', (done) => {
      chai.request(url)
        .get('/orders/obtener-ordenes/df')
        .send()
        .end( function(err,res){
          console.log(res.body)
        expect(res.body).to.be.an('array').that.is.empty;
        expect(res).to.have.status(200);
          done();
        });
    });

});
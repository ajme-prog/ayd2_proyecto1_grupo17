let chai = require('chai');
const expect = require('chai').expect;

let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const url= `http://35.222.169.75:4000/py1`;

describe('users API', function() {
    before(function() {
      // runs once before the first test in this block
    });
  
    after(function() {
      // runs once after the last test in this block
    });
  
    beforeEach(function() {
      // runs before each test in this block
    });
  
    afterEach(function() {
      // runs after each test in this block
    });
  
    // =============  PRUEBA PARA LOGUEASE  -- EXITOSA!!
    it('-> Servicio1: Deberia hacer login exitosamente', (done) => {
      chai.request(url)
        .post('/login')
        .send({mail:'cristian@gmail.com', password: "hola123"})
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(200);
          done();
        });
    });

    // =============  PRUEBA PARA LOGUEASE CON USUARIO INEXISTENTE -- FALLA!!
    it('-> Servicio1: Deberia fallar el login por usuario inexistente', (done) => {
      chai.request(url)
        .post('/login')
        .send({mail:'no existe', password: "hola123"})
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(500);
          done();
        });
    });

    // =============  PRUEBA PARA LOGUEASE CON CONTRASEÑA INCORRECTA -- FALLA!!
    it('-> Servicio1: Deberia fallar el login por contrasenia incorrecta', (done) => {
      chai.request(url)
        .post('/login')
        .send({mail:'cristian@gmail.com', password: "esta no es la contrasenia"})
        .end( function(err,res){
          console.log(res.body)
        expect(res).to.have.status(409);
          done();
        });
    });



});
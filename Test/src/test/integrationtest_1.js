const { describe, it, after, before } = require('mocha');

const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const Page = require('../lib/functions');
process.on('unhandledRejection', () => {});

(async function example() {
    try {
        describe('Pruebas de integracion en: >>' + process.env.HUB_HOST + "<< para >>"+`http://35.222.169.75:5000/`+'<<', function() {
            this.timeout(50000);
            let driver, page;

            beforeEach(async() => {
            // runs before each test in this block
                page = new Page();
                driver = page.driver;
                await page.visit(`http://35.222.169.75:5000/`);

            });

            afterEach(async() => {
            // runs after each test in this block
                await page.quit();
            });            
        
            it('Autenticación automática de usuario', async() => {                
                const result = await page.authenticationTrue('logIn-name','logIn-password','btn_account_sm','btn_log_in_entrar_salir','david@gmail.com','hola123');
                var obj = JSON.parse(result);
                expect(obj.idUsuario).to.equal(6);
            });
            
        });
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {
        
    }
})();
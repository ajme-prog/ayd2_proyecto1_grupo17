let chai = require('chai');
const expect = require('chai').expect;

let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const url= `http://35.222.169.75:4010/py1`;

describe('products API', function() {
  before(function() {
    // runs once before the first test in this block
  });
  
  after(function() {
    // runs once after the last test in this block
  });
  
  beforeEach(function() {
    // runs before each test in this block
  });
  
  afterEach(function() {
    // runs after each test in this block
  });
  
  // test cases
  it('deberia obtener los productos de forma exitosa', (done) => {
    chai.request(url)
      .get('/product')
      .end( function(err,res){
        console.log(res.body)
      expect(res).to.have.status(200);
        done();
      });
  });
});

//----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener todos los restaurantes: ',()=>{
  it('Deberia obtener todos los restaurantes', (done) => {
    chai.request(url)
    .get('/restaurante')
    .end( function(err,res){
      console.log(res.body)
    expect(res).to.have.status(200);
      done();
    });
  });
});
   

   //----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener todos los productos: ',()=>{
  it('Deberia obtener todos los productos de la BD', (done) => {
    chai.request(url)
    .get('/product')
    .end( function(err,res){
      console.log(res.body)
    expect(res).to.have.status(200);
      done();
    });
  });
});

   //-------------prueba obtener los productos de un restaurante
      //----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener los productos de un restaurante especifico: ',()=>{
  it('Deberia obtener todos los productos de un restaurante', (done) => {
    chai.request(url)
    .get('/product/1')
    .end( function(err,res){
      console.log(res.body)
    expect(res).to.have.status(200);
      done();
    });
  });
});
   
//-----------FALLIDA DELETE PRODUCTO
/*
describe('Eliminar un producto especifico: ',()=>{
  it('Deberia eliminar un producto', (done) => {
    chai.request(url)
    .delete('/product')
    .send({idProducto:"200"})
    .end( function(err,res){
      console.log(res.body)
    expect(res).to.have.status(200);
      done();
    });
  });
});
*/
   
//-----------EXITOSA DELETE PRODUCTO
describe('Eliminar un producto especifico prueba buena: ',()=>{
  it('Deberia eliminar un producto', (done) => {
    chai.request(url)
    .delete('/product')
    .send({idProducto:-1,id_usuario:2})
    .end( function(err,res){
      console.log(res.body)
    expect(res).to.have.status(200);
      done();
    });
  });
});
   
   
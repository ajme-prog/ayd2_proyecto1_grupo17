from fabric.api import *
import os

env.hosts = ['52.91.112.217','34.207.180.229']
env.user = 'ubuntu' # default is the local user
env.key_filename = './ayd2-prod.pem' # can be used both for SSH authentication and sudo
env.command_prefixes=["export CI_REGISTRY='"+str(os.environ.get('CI_REGISTRY'))+"'", 
    "export CI_JOB_TOKEN='"+str(os.environ.get('CI_JOB_TOKEN'))+"'", 
    "export VERSION='"+str(os.environ.get('VERSION'))+"'"]

@task
def deploy():
    run('mkdir -p testask')
    run(r'echo $CI_REGISTRY')
    run(r'echo $CI_JOB_TOKEN')
    run(r'echo $VERSION')
    run('docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY')
    put('docker-compose-production.yaml','~/docker-compose-production.yaml')
    run('docker-compose -f "docker-compose-production.yaml" up --build -d')
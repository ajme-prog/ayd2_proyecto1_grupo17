const connection = require('../services/dbmysql')

async function delete_detalle_ordenDB(idUsuario, idOrden, parameters) {
    const queryDeleteDetalle = `CALL deleteDetalleOrdenProducto(?,?,?,?)`;
    
    return await( connection.query(queryDeleteDetalle,parameters) );
}

async function delete_ordenDB(idUsuario, idOrden, parameters) {
    const queryDeleteOrden = `CALL deleteOrden(?,?,?,?)`;

    return await( connection.query(queryDeleteOrden,parameters) );
}


module.exports = {
    delete_detalle_ordenDB,
    delete_ordenDB
};
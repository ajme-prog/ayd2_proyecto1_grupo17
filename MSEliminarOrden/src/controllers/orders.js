const connection = require('../services/dbmysql');
const {
    insertErrorSistema,
    insertErrorUsuario
} = require('../services/log');
const { delete_detalle_ordenDB,
    delete_ordenDB } = require("../db_apis/ordersDB");

const {nombreServidor} = require('../services/data');

async function deleteOrder(req,res) {
    let idUsuario = parseInt(req.query.id_usuario);
    let idOrden = 0;
    const regex = /^[0-9]*$/;
    const isNumber = regex.test(req.query.id_orden);

    if(!isNumber){
        insertErrorUsuario(connection,['Error! se requiere un id de orden valido',JSON.stringify(req.query),idUsuario]);
        return res.status(404).send({ status:404, message: "Error no se pudo eliminar la orden" })
    }
    
    idOrden = parseInt(req.query.id_orden);

    const origen = nombreServidor.nombre;
    const descripcionDetalle = `Se eliminaron todos los productos asociados a la orden con el cod ${idOrden}`;
    const descripcionOrden = `Se elimino la orden con el cod ${idOrden}, por el usuario con el cod ${idUsuario}`;

    let parameters = [];
    parameters.push(idOrden);
    parameters.push(idUsuario);
    parameters.push(origen);
    parameters.push(descripcionDetalle);

    // VOY A ELIMINAR EL DETALLE PRIMERO
    try {
        await delete_detalle_ordenDB(idUsuario, idOrden, parameters);
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err ));
        return res.status(404).send({ status:404, message: "Error no se pudo eliminar el detalle de la orden" })
    }

    parameters[3] = descripcionOrden;
    try {
        const rows = await delete_ordenDB(idUsuario, idOrden, parameters);
        return res.status(200).send({status:200,message:"Orden eliminada correctamente"});
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        return res.status(404).send({status:404, message: "Error no se pudo eliminar la orden" })
    }

}

module.exports = {
    deleteOrder
};
const router = require('express').Router();
const { deleteOrder } = require('../controllers/orders');

router.delete('',deleteOrder);   // ELIMINAR UNA ORDEN CON TODO Y SU DETALLE


module.exports = router; 
# AyD2_Proyecto1_Grupo17
## Comandos utilizados para levantar el contendor de base de datos
docker run -dit -p 3306:3306 -p 33060:33060 --name mysql-db  -e  MYSQL_ROOT_PASSWORD=root  mysql  

docker build -t mydb:1.0 .  
docker run -dit -p 3306:3306 -p 33060:33060 --name mysql-db  -e MYSQL_ROOT_PASSWORD=root  mydb:1.0  

docker login  
docker build -t diegocabal/analisisdb:1.0 .  
docker run -dit -p 3306:3306 -p 33060:33060 --name mysql-db  diegocabal/analisisdb:1.0  
docker run -dit -p 3306:3306 -p 33060:33060 --name mysql-db --mount src=mysql-db-data,dst=/var/lib/mysql diegocabal/analisisdb:1.0  

### ELIMINAR LOS VOLUMENES CREADOS  

docker volume prune  

### CREAR VOLUMENES  

docker volume create mysql-db-data   

### ENLISTAR VOLUMENES  

docker volume ls  

ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'root';   
flush privileges;  

SHOW TABLES;  

-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 34.94.68.69    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DireccionDomiciliaria`
--

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

/* ALTER USER 'root'
IDENTIFIED WITH mysql_native_password BY 'root';
flush privileges; */


-- CREACION DE LA TABLA RESTAURANTE

DROP TABLE IF EXISTS `Restaurante`;
CREATE TABLE `Restaurante` (
  `idRestaurante` int NOT NULL AUTO_INCREMENT,
  `nombreRestaurante` varchar(150) DEFAULT NULL,
  `logo` varchar(500) DEFAULT NULL,
  `eliminado` BLOB,
  `Direccion_idDireccion` int DEFAULT NULL,
  PRIMARY KEY (`idRestaurante`),
  CONSTRAINT `fk_Restaurante_Direccion` 
    FOREIGN KEY (`Direccion_idDireccion`) 
    REFERENCES `DireccionDomiciliaria` (`idDireccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;





DROP TABLE IF EXISTS `DireccionDomiciliaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DireccionDomiciliaria` (
  `idDireccion` int NOT NULL AUTO_INCREMENT,
  `nombreDireccion` varchar(5000) DEFAULT NULL,
  `Usuario_idUsuario` int,
  PRIMARY KEY (`idDireccion`),
  KEY `fk_Direccion_Usuario` (`Usuario_idUsuario`),
  CONSTRAINT `fk_Direccion_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `Usuario` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Error`
--

DROP TABLE IF EXISTS `Error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Error` (
  `idError` int NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `nivelError` varchar(50) DEFAULT NULL,
  `origen` varchar(100) DEFAULT NULL,
  `descripcionError` varchar(100) DEFAULT NULL,
  `entradaError` varchar(100) DEFAULT NULL,
  `mensajeErrorSistema` varchar(1000) DEFAULT NULL,
  `Usuario_idUsuario` int DEFAULT NULL,
  PRIMARY KEY (`idError`),
  KEY `fk_Error_Usuario` (`Usuario_idUsuario`),
  CONSTRAINT `fk_Error_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `Usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Error`
--

LOCK TABLES `Error` WRITE;
/*!40000 ALTER TABLE `Error` DISABLE KEYS */;
/*!40000 ALTER TABLE `Error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Log`
--

DROP TABLE IF EXISTS `Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Log` (
  `idLog` int NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `tipoAccion` varchar(1) DEFAULT NULL,
  `origen` varchar(100) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `Usuario_idUsuario` int NOT NULL,
  PRIMARY KEY (`idLog`),
  KEY `fk_Log_Usuario` (`Usuario_idUsuario`),
  CONSTRAINT `fk_Log_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `Usuario` (`idUsuario`),
  CONSTRAINT `chk_tipoAccion` CHECK (((`tipoAccion` = _utf8mb4'0') or (`tipoAccion` = _utf8mb4'1') or (`tipoAccion` = _utf8mb4'2')))
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Orden`
--

DROP TABLE IF EXISTS `Orden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orden` (
  `idOrden` int NOT NULL AUTO_INCREMENT,
  `Usuario_idUsuario` int NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `tipoOrden` tinyint(1) DEFAULT NULL,
  `estadoOrden` varchar(1) DEFAULT NULL,
  `Direccion_idDireccion` int DEFAULT NULL,
  `Restaurante_idRestaurante` int NOT NULL,
  PRIMARY KEY (`idOrden`),
  KEY `fk_Orden_Usuario` (`Usuario_idUsuario`),
  KEY `fk_Orden_Direccion` (`Direccion_idDireccion`),
  CONSTRAINT `fk_Orden_Direccion` FOREIGN KEY (`Direccion_idDireccion`) REFERENCES `DireccionDomiciliaria` (`idDireccion`),
  CONSTRAINT `fk_Orden_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `Usuario` (`idUsuario`),
  CONSTRAINT `fk_Orden_Restaurante` 
    FOREIGN KEY (`Restaurante_idRestaurante`) 
    REFERENCES `Restaurante` (`idRestaurante`),
  CONSTRAINT `chk_estadoOrden` CHECK (((`estadoOrden` = _utf8mb4'0') or (`estadoOrden` = _utf8mb4'1') or (`estadoOrden` = _utf8mb4'2') or (`estadoOrden` = _utf8mb4'3') or (`estadoOrden` = _utf8mb4'4') or (`estadoOrden` = _utf8mb4'5')))
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Producto`
--

DROP TABLE IF EXISTS `Producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Producto` (
  `idProducto` int NOT NULL AUTO_INCREMENT,
  `nombreProducto` varchar(50) DEFAULT NULL,
  `precio` decimal(13,2) unsigned DEFAULT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `oferta` int DEFAULT 0,
  `Restaurante_idRestaurante` int NOT NULL,
  PRIMARY KEY (`idProducto`),
  CONSTRAINT `fk_Producto_Restaurante` 
    FOREIGN KEY (`Restaurante_idRestaurante`) 
    REFERENCES `Restaurante` (`idRestaurante`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Usuario` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(50) DEFAULT NULL,
  `apellidoUsuario` varchar(50) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `idAvatar` varchar(200) DEFAULT NULL,
  `tipoUsuario` varchar(1) DEFAULT NULL,
  `Restaurante_idRestaurante` int,
  PRIMARY KEY (`idUsuario`),
  CONSTRAINT `fk_Usuario_Restaurante` 
    FOREIGN KEY (`Restaurante_idRestaurante`) 
    REFERENCES `Restaurante` (`idRestaurante`),
  CONSTRAINT `chk_tipoUsuario` CHECK (((`tipoUsuario` = _utf8mb4'0') or (`tipoUsuario` = _utf8mb4'1') or (`tipoUsuario` = _utf8mb4'2')))
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `detalleOrdenProducto`
--

DROP TABLE IF EXISTS `detalleOrdenProducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalleOrdenProducto` (
  `Producto_idProducto` int NOT NULL,
  `Orden_idOrden` int NOT NULL,
  `cantidadProducto` int DEFAULT NULL,
  `precio` decimal(13,2) DEFAULT NULL,
  KEY `fk_detalleOrdenProducto_Producto` (`Producto_idProducto`),
  KEY `fk_detalleOrdenProducto_Orden` (`Orden_idOrden`),
  CONSTRAINT `fk_detalleOrdenProducto_Orden` FOREIGN KEY (`Orden_idOrden`) REFERENCES `Orden` (`idOrden`),
  CONSTRAINT `fk_detalleOrdenProducto_Producto` FOREIGN KEY (`Producto_idProducto`) REFERENCES `Producto` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;




--
-- Dumping routines for database 'mydb'
--
/*!50003 DROP FUNCTION IF EXISTS `insertOrden` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `insertOrden`(
	idUsuario INT,
	tipoO BOOLEAN,
	estadoO VARCHAR(1),
    idDireccion INT,
    idRestaurante INT,
	origenLog VARCHAR(100),
	descripcionLog VARCHAR(200)
) RETURNS int
    DETERMINISTIC
BEGIN 
	DECLARE id INT;
	INSERT INTO Orden(Usuario_idUsuario,fecha,tipoOrden,estadoOrden,Direccion_idDireccion,Restaurante_idRestaurante)
	VALUES(idUsuario,NOW(),tipoO,estadoO,idDireccion,idRestaurante);
    
    SET id = LAST_INSERT_ID();
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '0', origenLog, descripcionLog, idUsuario);
    
    RETURN id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `insertProducto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `insertProducto`( 
	nombreP VARCHAR(50),
	precioP DECIMAL(13,2),
	imagenP VARCHAR(500),
  idRestaurante INT,
    idUsuario INT,
    origenLog VARCHAR(100),
    descripcionLog VARCHAR(200)
) RETURNS int
    DETERMINISTIC
BEGIN 
	DECLARE id int;
	INSERT INTO Producto(nombreProducto,precio,imagen,Restaurante_idRestaurante)
	VALUES(nombreP,precioP,imagenP,idRestaurante);
    SET id = LAST_INSERT_ID();
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '0', origenLog, descripcionLog, idUsuario);
    RETURN id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteDetalleOrdenProducto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteDetalleOrdenProducto`(
    IN idD INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  DELETE FROM detalleOrdenProducto
  WHERE Orden_idOrden = idD;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteOrden` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteOrden`(
    IN idO INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  DELETE FROM Orden
  WHERE idOrden = idO;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deleteProducto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteProducto`(
    IN idP INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  DELETE FROM Producto
  WHERE idProducto = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertUsuario`(
	IN nombreU VARCHAR(50),
	IN apellidoU VARCHAR(50),
	IN passU VARCHAR(1000),
	IN correoU VARCHAR(100),
    IN direccion VARCHAR(100),
	IN idAvatarU VARCHAR(200),
	IN tipoU VARCHAR(1),
  IN idRestaurante INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
	DECLARE ultimoid INT;
	INSERT INTO Usuario (nombreUsuario,apellidoUsuario,password,correo,idAvatar,tipoUsuario, Restaurante_idRestaurante)
	VALUES(nombreU,apellidoU,passU,correoU,idAvatarU,tipoU,idRestaurante);
	
	SELECT idUsuario INTO ultimoid
	FROM Usuario
	ORDER BY idUsuario DESC
	LIMIT 1;

	INSERT INTO DireccionDomiciliaria (nombreDireccion,Usuario_idUsuario)
	VALUES(direccion,ultimoid);

	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '0', origenLog, descripcionLog, ultimoid);
    INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '0', 'Servicio1', 'Se registro direccion', ultimoid);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateEstadoOrden` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateEstadoOrden`(
	idOrdenUP INT,
	idUsuario INT,
	estadoO VARCHAR(1),
	origenLog VARCHAR(100),
	descripcionLog VARCHAR(200)
)
BEGIN 
    UPDATE Orden
	SET estadoOrden = estadoO
	WHERE idOrden = idOrdenUP;
    
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '1', origenLog, descripcionLog, idUsuario);
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateProducto1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateProducto1`(
    IN idP INT,
	IN nombreP VARCHAR(50),
	IN precioP DECIMAL(13,2) UNSIGNED,
	IN imagenP VARCHAR(500),
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Producto
  SET nombreProducto = nombreP, precio = precioP, imagen = imagenP
  WHERE idProducto = idP;

	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '1', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updateProducto2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateProducto2`(
    IN idP INT,
	IN nombreP VARCHAR(50),
	IN precioP DECIMAL(13,2) UNSIGNED,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Producto
  SET nombreProducto = nombreP, precio = precioP
  WHERE idProducto = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '1', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- procedimiento para poner de oferta un producto
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateOfertaProducto`(
    IN idP INT,
    IN ofertaP INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Restaurante
  SET oferta = ofertaP
  WHERE idRestaurante = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;



-- procedimiento para insertar un nuevo restaurante
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `insertRestaurante`(
    IN nombreP VARCHAR(150),
	IN logoP VARCHAR(500),
	IN idDireccion INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN
    INSERT INTO Restaurante(nombreRestaurante, logo, eliminado, Direccion_idDireccion)
    VALUES(nombreP,logoP, false,idDireccion);

	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '1', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;


-- procedimiento para eliminar un restaurante
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `deleteRestaurante`(
    IN idP INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Restaurante
  SET eliminado = true
  WHERE idRestaurante = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;


-- procedimiento para actualizar los datos de un restaurante con logo
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateRestauranteconLogo`(
    IN idP INT,
	  IN nombreP VARCHAR(150),
	IN logoP VARCHAR(500),
	IN idDireccion INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Restaurante
  SET nombreRestaurante = nombreP, logo = logoP, idDireccion = idDireccion
  WHERE idRestaurante = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;


-- procedimiento para actualizar los datos de un restaurante sin logo
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `updateRestaurantesinLogo`(
    IN idP INT,
	  IN nombreP VARCHAR(150),
	IN idDireccion INT,
	IN idUsuario INT,
	IN origenLog VARCHAR(100),
	IN descripcionLog VARCHAR(200)
)
BEGIN 
  UPDATE Restaurante
  SET nombreRestaurante = nombreP, idDireccion = idDireccion
  WHERE idRestaurante = idP;
  
	INSERT INTO Log (fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
	VALUES(NOW(), '2', origenLog, descripcionLog, idUsuario);

  COMMIT;
END ;;
DELIMITER ;
-- Dump completed on 2021-03-09 23:43:27


-- DATA
    
INSERT INTO DireccionDomiciliaria(idDireccion, nombreDireccion)
VALUES
    (6,'cf85fbf587b9fdff5f1a94a85c1026e5:cf2d2387656ce2f2ad60c7f74f87e509');
    

INSERT INTO Restaurante(idRestaurante, nombreRestaurante, logo, eliminado, Direccion_idDireccion)
VALUES
  (1,'GuateFood','producto/grid_KungPaoChicken.jpg',false,6);
  


INSERT INTO Usuario(idUsuario, nombreUsuario, apellidoUsuario, password, correo, idAvatar, tipoUsuario) 
VALUES
	(1,'Consumidor final',null,null,null,null,2),
	(2,'Cristian',	'Castellanos',	'786c2a5970483df6644a882545da8e32:cd71045fe706fd','cristian@gmail.com','cris',1),
    (3,'Alan',		'Morataya',		  '786c2a5970483df6644a882545da8e32:cd71045fe706fd','alan@gmail.com','alan',1),
    (4,'Diego',		'Caballeros',	  '786c2a5970483df6644a882545da8e32:cd71045fe706fd','diego@gmail.com','diego',1),
    (5,'Carlos',	'Garcia',       '786c2a5970483df6644a882545da8e32:cd71045fe706fd','carlos@gmail.com','carlos',1);

INSERT INTO Usuario(idUsuario, nombreUsuario, apellidoUsuario, password, correo, idAvatar, tipoUsuario, Restaurante_idRestaurante) 
VALUES
    (6,'David',		'Enriquez',		  '786c2a5970483df6644a882545da8e32:cd71045fe706fd','david@gmail.com','david',0,1);


INSERT INTO DireccionDomiciliaria(idDireccion, nombreDireccion, Usuario_idUsuario)
VALUES
    (1,'cf85fbf587b9fdff5f1a94a85c1026e5:cf2d2387656ce2f2ad60c7f74f87e509',2),
    (2,'71878e2b1a2a623db49f7d0e15ae551d:07be1ad2a6c2d2b731ecf71cb4876b70',3),
    (3,'91fbe54606bbd2e19eaffa8e688b2412:610e49be0204e3f9c0560504a1805d59',4),
    (4,'bef719fcdc5bb5b7ce5088218f18cb3e:35664d822a630592c648b9343fdf875a',5),
    (5,'a8b0fc4234cb691978f4acd7c93b6ddf:ac47d858a8ac530cef3e5bd695772f6c',6);

INSERT INTO Log(fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
VALUES
	(NOW(),0,'Servicio1','Se registro un usuario',2),
    (NOW(),0,'Servicio1','Se registro direccion',2),
    (NOW(),0,'Servicio1','Se registro un usuario',3),
    (NOW(),0,'Servicio1','Se registro direccion',3),
    (NOW(),0,'Servicio1','Se registro un usuario',4),
    (NOW(),0,'Servicio1','Se registro direccion',4),
    (NOW(),0,'Servicio1','Se registro un usuario',5),
    (NOW(),0,'Servicio1','Se registro direccion',5),
    (NOW(),0,'Servicio1','Se registro un usuario',6),
    (NOW(),0,'Servicio1','Se registro direccion',6);

INSERT INTO Producto(idProducto, nombreProducto, precio, imagen, Restaurante_idRestaurante)
VALUES
	(1,'Orange Chicken',35.50,'producto/grid_OrangeChicken.jpg',1),
    (2,'Kung Pao Chicken ',25.50,'producto/grid_KungPaoChicken.jpg',1),
    (3,'String Bean Chicken',45.25,'producto/grid_StringBeanChicken.jpg',1),
    (4,'Mushroom Chicken',21.50,'producto/grid_MushroomChicken.jpg',1),
    (5,'Sweet Fire Chicken',48.75,'producto/grid_SweetFireChicken.jpg',1);
    
INSERT INTO Log(fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
VALUES
	(NOW(),0,'Servicio2','New producto added Orange Chicken',2),
    (NOW(),0,'Servicio2','New producto added Kung Pao Chicken',2),
    (NOW(),0,'Servicio2','New producto added String Bean Chicken',2),
    (NOW(),0,'Servicio2','New producto added Mushroom Chicken',2),
    (NOW(),0,'Servicio2','New producto added Sweet Fire Chicken',2);

INSERT INTO Orden(idOrden, Usuario_idUsuario, fecha, tipoOrden, estadoOrden, Direccion_idDireccion, Restaurante_idRestaurante)
VALUES
-- Todas las ordenes ingresadas son nuevas
	(1,3, NOW(),0,0,2,1),
    (2,4, NOW(),0,0,3,1),
    (3,3, NOW(),0,0,2,1),
    (4,4, NOW(),0,0,3,1),
    (5,3, NOW(),0,0,2,1);

INSERT INTO detalleOrdenProducto()
VALUES
	(1,1,	1,35.50),
    (2,2,	1,25.50),
    (3,3,	1,45.25),
    
    (4,4,	1,21.50),
    (1,4,	1,35.50),
    
    (2,5,	2,25.50),
    (5,5,	1,48.75),
    (4,5,	3,21.50);

INSERT INTO Log(fecha,tipoAccion,origen,descripcion,Usuario_idUsuario)
VALUES
	(NOW(),0,'Microservicio Crear Orden','Se creo una nueva orden de tipo false, con un estado inicial 0 y con 1 productos',3),
    (NOW(),0,'Microservicio Crear Orden','Se creo una nueva orden de tipo false, con un estado inicial 0 y con 1 productos',4),
    (NOW(),0,'Microservicio Crear Orden','Se creo una nueva orden de tipo false, con un estado inicial 0 y con 1 productos',3),
    (NOW(),0,'Microservicio Crear Orden','Se creo una nueva orden de tipo false, con un estado inicial 0 y con 2 productos',4),
    (NOW(),0,'Microservicio Crear Orden','Se creo una nueva orden de tipo false, con un estado inicial 0 y con 3 productos',3);




/*
SELECT * FROM Error;
SELECT * FROM Log;
SELECT * FROM detalleOrdenProducto;
SELECT * FROM Orden;
SELECT * FROM Producto;
SELECT * FROM DireccionDomiciliaria;
SELECT * FROM Usuario;
*/

/*
DELETE FROM Error;
DELETE FROM Log;
DELETE FROM detalleOrdenProducto;
DELETE FROM Orden;
DELETE FROM Producto;
DELETE FROM DireccionDomiciliaria;
DELETE FROM Usuario;
*/
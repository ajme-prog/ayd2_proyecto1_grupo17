import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { ThemeProvider } from '@material-ui/core/styles';
import { unstable_createMuiStrictModeTheme } from '@material-ui/core/styles';


const theme = unstable_createMuiStrictModeTheme(
  {
    palette: {
      background: {
        default: "#222222",
      },
      primary: {
        light: '#ffad42',
        main: '#f57c00',
        dark: '#bb4d00',
        contrastText: '#ffffff',
      },
      secondary: {
        light: '#ff5131',
        main: '#d50000',
        dark: '#9b0000',
        contrastText: '#ffffff',
      },
    },
  }
  );

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App theme={theme}/>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

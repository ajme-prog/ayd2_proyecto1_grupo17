import React from 'react';
import  Grid  from '@material-ui/core/Grid';
import  Paper  from '@material-ui/core/Paper';
import MUIDataTable from "mui-datatables";

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Alert from "@material-ui/lab/Alert";


import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import PublishIcon from '@material-ui/icons/Publish';
import axios from 'axios';
import ConfirmDialog from './ConfirmDialog2'
import { Hidden } from '@material-ui/core';

const BASE_URL = 'http://localhost'
const img_BASE_URL_S3 = 'https://productos-ayd2.s3.us-east-2.amazonaws.com/'

class CRUDProducts extends React.Component{
    constructor(props){
        super(props)
        this.state={
            indexOfRowSelected:-1,
            imgB64:'',
            imgExtension:'',
            image:'',
            haveOneRowSelected:false,
            columns:["ID","Nombre", "Precio","Precio ofertado"],
            
            data:[
                ["1","Pizza", "10"],
                ["2","Fideos", "20"],
                ["3","Salchipapas", "30"],
                ["4","Ramen", "40"],
               ],
            options:{
                filterType: 'checkbox',
                customToolbarSelect:()=>{return(<div></div>)},
                selectableRowsHeader:false,
                selectableRows:'single',
                onRowSelectionChange:(a,b,c)=>{this.onRowSelectionChange(a,b,c)},
                rowsSelected:[]
            },
            txtProductNameValue:'',
            txtProductPriceValue:'',
            txtProductDescountRateValue:'',
            editProductImageChanged:false,
            txtProductPriceLabelProps:{},
            txtProductPriceLabelProps:{},
            txtProductDescountRateLabelProps:{},
            valuee: '',
            dialogIsOpen:false,
        }
        // Methods
        this.uploadImageOnChange = this.uploadImageOnChange.bind(this)
        this.func_getAllProducts = this.func_getAllProducts.bind(this)
        this.func_addProduct = this.func_addProduct.bind(this)
        this.func_deleteProduct = this.func_deleteProduct.bind(this)
        this.func_editProduct = this.func_editProduct.bind(this)
        this.handleKeyUp=this.handleKeyUp.bind(this)
        this.func_clearForm = this.func_clearForm.bind(this)

        // Refs
        this.txtProductName = React.createRef()
        this.txtProductPrice = React.createRef()
        this.txtProductDescountRate = React.createRef()
        this.txtProductImg = React.createRef()
    }
    // Life Cycle
    componentDidMount(){
        this.func_getAllProducts()
    }
    func_clearForm(){
        this.txtProductName.current.value=""
        this.txtProductPrice.current.value=""
        this.txtProductDescountRate.current.value=""
        this.setState({
            image:''
        })
    }
    // Interact with APIs
    func_getAllProducts(){
        this.func_clearForm()
        console.log(this.props.BASE_URL)
        console.log('CRUDProducts - Obtener todos los productos')
        var idRestaurante = JSON.parse(localStorage.getItem('usr')).idRestaurante

        axios.get(this.props.BASE_URL+':4010/py1/product/'+idRestaurante+"")
        .then(res=>{
            console.log("AQUI")
            console.log(res)
            console.log(res.data)
            var arr = []
            var arr_from_db = res.data.payload
            arr_from_db.forEach(prod=>{
                // var num = 0
                // if(prod.oferta!=0){
                //     num=100-Number((((prod.oferta/prod.precio))*100))
                // }
                // var porcentaje_descuento = num.toFixed(2);
                // var rounded = Number(porcentaje_descuento);
                arr.push([
                    prod.idProducto,
                    prod.nombreProducto,
                    prod.precio,
                    prod.oferta,
                    prod.imagen,
                ])
            })

            this.setState({data:arr})
        })
    }
    func_addProduct(){
        if(this.txtProductName.current.value=="" || this.txtProductPrice.current.value==""){
          
            this.setState({vacio:true})
            setTimeout(() => {
               this.setState({vacio:false})
               
            }, 3000); 
   
              return;
             }

        var usr = JSON.parse(localStorage.getItem('usr'))
        console.log(usr)
        console.log(usr.idUsuario)
        const id_Usuario = usr.idUsuario
        const name = this.txtProductName.current.value
        const price = parseFloat(this.txtProductPrice.current.value)
        var imgBase64 = this.state.imgB64
        var startOfB64 = imgBase64.indexOf(',')
        imgBase64 = imgBase64.substr(startOfB64,imgBase64.length)
        const extension = this.state.imgExtension
        var id_restau = usr.idRestaurante
            console.log(id_restau)
        var product = {
            user_id:id_Usuario,
            name:name,
            precio:price,
            extension:extension,
            payload:imgBase64,
            id_restaurante:id_restau
        }
        console.log(product)
        console.log(JSON.stringify(product))
        var that = this;
        axios.post(this.props.BASE_URL+':4010/py1/product', product).then(res=>{
            console.log(res)
            console.log(res.data)
            if(res.status==200){
                this.props.displayNotification('success','Producto agregado exitosamente')
            }else{
                this.props.displayNotification('error','Error al intentar agregar producto')
            }
            console.log('Agregado')
            this.func_getAllProducts()
        })
        
    }
    func_deleteProduct(){
        var usr = JSON.parse(localStorage.getItem('usr'))
        console.log(this.state.data[this.state.indexOfRowSelected.dataIndex])
        var idProd = this.state.data[this.state.indexOfRowSelected.dataIndex][0]
        var prod_to_delete = {
            idProducto:idProd,
            id_usuario:usr.idUsuario
        }
        console.log(prod_to_delete)
        axios.delete(this.props.BASE_URL+':4010/py1/product',
        {data:{"idProducto":idProd,"id_usuario":usr.idUsuario}})
        .then(res=>{
            console.log(res)
            console.log(res.data)
            
            if(res.status==200){
                this.props.displayNotification('info','Producto eliminado')
            }
            else{
                this.props.displayNotification('error','Error al elimianr producto')
            }
            this.setState({haveOneRowSelected:false})

            this.func_getAllProducts()
        })
    }
    func_editProduct(){
        // {
        //     "name":"B",
        //     "precio":233,
        //     "extension":"jpg",
        //     "id_usuario":4,
        //     "payload":"XDDDDDDDDD"
        //  }
        var nameProd = this.txtProductName.current.value
        var priceProd = this.txtProductPrice.current.value
        var descountProd = this.txtProductDescountRate.current.value
        
        priceProd = parseFloat(priceProd)
        // var oferta = priceProd-(priceProd*(descountProd/100))
        var oferta = parseFloat(descountProd)
        console.log("OFERTA")
        console.log(oferta)
        var imgBase64 = this.state.imgB64
        var startOfB64 = imgBase64.indexOf(',')
        imgBase64 = imgBase64.substr(startOfB64,imgBase64.length)
        const extension = this.state.imgExtension


        var usr = JSON.parse(localStorage.getItem('usr'))
        var prod_edited = {}
        var idProd = this.state.data[this.state.indexOfRowSelected.dataIndex][0]
        if(this.state.editProductImageChanged){
            console.log('Se cambio imagen')
            prod_edited = {
                name:nameProd,
                precio:priceProd,
                oferta:oferta,
                id_usuario:usr.idUsuario,
                extension:extension,
                payload:imgBase64
    
            }
            this.setState({
                editProductImageChanged:false
            })
        }else{
            console.log('No se cambio imagen')
            prod_edited = {
                name:nameProd,
                precio:priceProd,
                oferta:oferta,
                id_usuario:usr.idUsuario,
                extension:'',
                payload:''
            }
        }
        console.log(idProd)
        console.log(prod_edited)
        axios.post(this.props.BASE_URL+':4010/py1/product/'+idProd, prod_edited).then(res=>{
            console.log(res)
            console.log(res.data)
            if(res.status==200){
                this.props.displayNotification('success','Producto editado exitosamente')
            }else{
                this.props.displayNotification('error','Error al intentar editar producto')
            }

            console.log('Actualizado')
            this.func_getAllProducts()
        })
    }
    // Utilities
    onRowSelectionChange(currentRowsSelected,allRowsSelected,rowsSelected){
        console.log(currentRowsSelected)
        console.log(allRowsSelected)
        console.log(rowsSelected)
        this.state.indexOfRowSelected=currentRowsSelected[0]
        console.log(this.state.data)
        console.log(this.state.indexOfRowSelected.dataIndex)
        
        this.txtProductName.current.value = this.state.data[this.state.indexOfRowSelected.dataIndex][1]
        this.txtProductPrice.current.value = this.state.data[this.state.indexOfRowSelected.dataIndex][2]
        this.txtProductDescountRate.current.value = this.state.data[this.state.indexOfRowSelected.dataIndex][3]
        this.state.image = img_BASE_URL_S3+""+this.state.data[this.state.indexOfRowSelected.dataIndex][4]
        console.log(this.state.image)
        if(rowsSelected.length==1){
            this.setState({
                haveOneRowSelected:true,
                rowsSelected:rowsSelected,
                image:this.state.image,
//                txtProductNameValue:A,
//                txtProductPriceValue:B
                txtProductNameLabelProps:{shrink:true},
                txtProductPriceLabelProps:{shrink:true},
                txtProductDescountRateProps:{shrink:true}
            })
        }else{
            this.txtProductName.current.value = ''
            this.txtProductPrice.current.value = ''
            this.txtProductDescountRate.current.value=""
            this.state.image = ''
    

            this.setState({
                haveOneRowSelected:false,
                rowsSelected:[],
                txtProductPriceValue:'',
                txtProductNameValue:'',
                txtProductDescountRate:'',
                image:'',
                txtProductNameLabelProps:{shrink:false},
                txtProductPriceLabelProps:{shrink:false},
                txtProductDescountRateProps:{shrink:false}
            })
        }
    }
    uploadImageOnChange(event){
        if(this.state.haveOneRowSelected){
            this.setState({
                editProductImageChanged:true
            })
            // console.log('Editando pero cambio de imagen')
            // var img = event.target
            // console.log(img)
            // var canvas = document.createElement("canvas");
            // canvas.width = img.width;
            // canvas.height = img.height;
            // var ctx = canvas.getContext("2d");
            // ctx.drawImage(img, 0, 0);
            // var dataURL = canvas.toDataURL("image/png");
            
            // that.state.imgB64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            // //Extension
            // var lastIndexOfDot = event.target.src.lastIndexOf('.')

            // this.state.imgExtension = fileName.substr(lastIndexOfDot,fileName.length)
        }

        var fileName = event.target.files[0].name
        var lastIndexOfDot = fileName.lastIndexOf('.')

        this.state.imgExtension = fileName.substr(lastIndexOfDot,fileName.length)
        console.log(this.state.imgExtension)
        this.setState({image:URL.createObjectURL(event.target.files[0])})
        var FR= new FileReader();
        var that = this
        FR.addEventListener("load", function(e) {
            that.state.imgB64 = e.target.result
        })
        FR.readAsDataURL(event.target.files[0]);
    

    }
    handleKeyUp(event) {
        console.log('fd')
        console.log(event.key)
        console.log(event.charCode)
        if(event.key=='1'|| event.key=='2' || event.key=='3' ||event.key=='4'||event.key=='5'|| event.key=='6' || event.key=='7' || event.key=='8' || event.key=='9' ||event.key=='0' ){
            
        }else if(event.key=='Backspace'){
            
        }else if(event.key=='.'){
            var indices = [];
            for(var i = 0; i < this.txtProductPrice.current.value.length; i++) {
	            if (this.txtProductPrice.current.value[i].toLowerCase() === ".") indices.push(i);
            }
            if(indices>2){
                this.txtProductPrice.current.value=''
            }else{ 
                
            }
        }else if(event.key=='-'){
            this.txtProductPrice.current.value=''
        }else{
            
        }
        
    }
    
    render(){
        return(

            <div>
                {this.state.vacio && (
   
   <Alert variant="filled" severity="warning">
    Necesita llenar todos los campos del formulario
   </Alert>
     
 )}

{this.state.img && (
   
   <Alert variant="filled" severity="warning">
    Es 
   </Alert>
     
 )}
                <Grid
                    container
                    spacing={2}
                    direction="row"
                    justify="space-around"
                    alignItems="stretch"
                >
                    <Grid
                    item
                    md={6}
                    >
                    <MUIDataTable
                        title={"Productos"}
                        data={this.state.data}
                        columns={this.state.columns}
                        options={this.state.options}
                    />
                    </Grid>
                    <Grid
                    item
                    md={6}
                    >
                        <Card elevation={3} >
                            <CardContent>
                                <Typography variant="h6" >
                                {this.state.haveOneRowSelected?"Editar o eliminar":"Agregar"} producto
                                </Typography>
                                <br/>
                                <Grid
                                    container
                                    spacing={2}
                                >
                                    <Grid item xs={12} md={6}>
                                        <TextField 
                                        autoFocus={true}
                                        InputLabelProps={this.state.txtProductNameLabelProps}
                                        inputRef={this.txtProductName}
                                        fullWidth id="prod_name" label="Nombre" variant="outlined" />
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <TextField 
                                        
                                        type="number"
                                        autoFocus={true}
                                        InputLabelProps={this.state.txtProductPriceLabelProps}
                                        InputProps={{ inputProps: { min: 0 } }}
                                        inputRef={this.txtProductPrice}
                                        fullWidth id="prod_name" label="Precio" variant="outlined"
                                        onKeyUp={this.handleKeyUp}
                                        />
                                        
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <TextField 
                                        disabled={!this.state.haveOneRowSelected}
                                        type="number"
                                        
                                        autoFocus={true}
                                        InputLabelProps={this.state.txtProductDescountRateProps}
                                        InputProps={{ inputProps: { min: 0 } }}
                                        inputRef={this.txtProductDescountRate}
                                        fullWidth id="prod_descount_rate" label="Precio de oferta" variant="outlined"
                                        helperText="NOTA: colocar 0 en este campo para cancelar la oferta"
                                        onKeyUp={this.handleKeyUp}
                                        />
                                    </Grid>
                                    <Grid item  xs={12} md={6}>
                                        <input
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                            style={{display:'none'}}
                                            onChange={this.uploadImageOnChange}
                                        />
                                        <label htmlFor="contained-button-file">
                                            <Button 
                                            id={"btn_productos_formulario_seleccionarImagen"}
                                            fullWidth variant="contained" color="primary" component="span" endIcon={<PublishIcon />}>
                                                Seleccionar imagen
                                            </Button>
                                        </label>
                                    </Grid>
                                    <Grid item xs={6}>

                                        <Typography variant="subtitle2" >
                                            Imagen seleccioanda:
                                        </Typography>
                                        <br/>
                                        <img width="120px" height="auto" maxHeight="50px" src={this.state.image}/>
                                    </Grid>

                                </Grid>
                            </CardContent>
                            <CardActions>
                                {this.state.haveOneRowSelected?
                                        <Grid container spacing={2}
                                            direction="row"
                                            justify="flex-end"
                                            alignItems="center"
                                        >
                                            <ConfirmDialog 
                                            onConfirm={this.func_deleteProduct}
                                            isOpen={this.state.dialogIsOpen}
                                            handleClose={()=>{this.setState({dialogIsOpen:false})}}
                                            title={'¿Esta seguro de eliminar este producto?'}
                                            subtitle={'Esta operación no se puede deshacer'}/>
                                            <Grid item>
                                                <Button 
                                                id={"btn_productos_formulario_editar"}
                                                onClick={this.func_editProduct}
                                                color="default" endIcon={<EditIcon />} variant="contained">Editar</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button 
                                            id={"btn_productos_formulario_eliminar"}
                                            onClick={()=>{this.setState({dialogIsOpen:true})}}
                                                color="secondary" endIcon={<DeleteIcon />} variant="contained">Eliminar</Button>
                                            </Grid>

                                        </Grid>
                                :
                                    <Grid container spacing={2} 
                                        direction="row"
                                        justify="flex-end"
                                        alignItems="flex-end"
                                    >
                                        <Grid item >
                                            <Button
                                            id={"btn_productos_formulario_agregar"}
                                            onClick={this.func_addProduct}
                                            color="primary" endIcon={<AddIcon />} variant="contained">Agregar</Button>
                                        </Grid>
                                    </Grid>
                                }
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        )
    }
}


export default CRUDProducts
import React from 'react';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import ControlPointIcon from '@material-ui/icons/ControlPoint';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';

export default function ItemInShoppingCart(props){

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      width: 500,
      height: 450,
    },
    icon: {
    color: '#d50000',
    }
  }));
  const classes = useStyles();
    return(
        <div>
            <Card>
                <CardContent>
                    <Grid
                    container spacing={3}
                    direction="row"
                    justify="space-around"
                    alignItems="center"
                    >
                        <Grid 
                        item xs={11} md={11}
                        >
                            <Typography variant="h5">
                                <label>
                                {props.quantity} - {props.name}
                                </label>
                            </Typography>
                            <Typography variant="h6">
                                <label>
                                {props.price}
                                </label>
                            </Typography>
                            <Grid
                                container
                                spacing={2}
                                >
                            <Grid item>
                                <Typography gutterBottom variant="subtitle2">
                                    De: {props.nombreRestaurante}
                                </Typography>

                            </Grid>
                            <Grid item>
                                <img style={{maxWidth:'30px',height:'auto'}} 
                                src={props.logo}/>
                            </Grid>

                        </Grid>

                            <Typography variant="caption" display="block" gutterBottom>
                                Subtotal: {props.subtotal}
                            </Typography>
                            <img width="120px" height="auto" src={props.img}/>


                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    
                    <Grid
                    container
                    direction="row"
                    justify="flex-end"
                    alignItems="center"                        
                    >
                        <Grid item xs={2}>
                            <IconButton 
                            onClick={()=>{
                                props.displayNotification('warning','Producto descartado del carrito')
                                props.handleCarContentQuantityChangeRemove(props.indexInArray)}}
                            aria-label={`Borrar`} className={classes.icon} >
                                <DeleteOutlineIcon />
                            </IconButton>
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton
                            onClick={()=>{props.handleCarContentQuantityChangeOne(props.indexInArray,-1)}}
                            aria-label={`Restar 1 ${props.name}`} >
                                <RemoveCircleOutlineIcon />
                            </IconButton>
                        </Grid>
                        <Grid item xs={1}>
                            <TextField
                                id={props.idProd+""}
                                value={props.quantity}
                                onChange={props.handleCarContentQuantityChange}
                                inputProps={{min: 0, style: { textAlign: 'center', color:'#c1c4c4' }}}  
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <IconButton 
                            onClick={()=>{props.handleCarContentQuantityChangeOne(props.indexInArray,1)}}                                
                            aria-label={`Adicionar 1 ${props.name}`} >
                                <ControlPointIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </div>
    )
}
import React, { useEffect, useState } from 'react';
const crypto = require('crypto');

export function useElements({BASE_URL}){
    
  const [usr,] = useState(JSON.parse(localStorage.getItem('usr')))
  const [orders,setOrders] = useState([])
  const [displayorders,setDisplayOrders] = useState([])
  const [notify,setNotify] = useState({isOpen:false, message: '', type:'' })
  const [confirmDialog,setConfirmDialog] = useState({isOpen:false,title:'',subtitle:''})


  const getOrdenes = () =>{
    fetch(BASE_URL+':3030/orders/obtener-ordenes/'+usr.idUsuario)
      .then(res => res.json())
      .then(json => {
        setOrders([...calculateTotal(json)])
        const aux = []
        //console.log(json)
        json.forEach(order => {
          const date = new Date(order.fecha);
          const datefmt = date.getDate()+"/"+(parseInt(date.getMonth())+1)+"/"+date.getFullYear()+" - "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()
          aux.push([datefmt,decrypt(order.direccion), order.total ])
        })
        setDisplayOrders([...aux])
      })
  }

  const cancelOrder = (id_orden) => {
      fetch(BASE_URL+':3000/orders/actualizar-estado-orden',{
        method:'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id_orden,
          id_usuario:usr.idUsuario,
          estado:"4"
          })
        })
        .then(res => res.json())
        .then(json => {
          if(json.status === 200){
            getOrdenes()
            setConfirmDialog({...confirmDialog,isOpen:false})
            setNotify({isOpen:true,message:'Orden cancelada con exito',type:'success'})
          } 
        })
  } 

  useEffect(()=>{
    if(usr!=null) getOrdenes()
  },[])

  return {
    orders,
    displayorders,
    cancelOrder,
    notification:{
      notify,
      setNotify
    },
    confirmDialogState:{
      confirmDialog,
      setConfirmDialog
    }
  }
}

function decrypt(text) {
  const algorithm = 'aes-256-ctr';
  const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  //console.log(decrypted.toString())
  return decrypted.toString();
}

function calculateTotal(orders){
  const aux = []
  let total = 0
  orders.forEach((order) => {
    total = 0
    order.productos.forEach(product => {
      total += product.cant_producto * product.precio
    });
    order.total = total.toFixed(2)
    aux.push(order)
  })
  return aux
}
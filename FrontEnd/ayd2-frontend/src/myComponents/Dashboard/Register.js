import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Imort cssp
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Alert from "@material-ui/lab/Alert";
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Swal from "sweetalert2";
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { FormControlLabel,Switch} from '@material-ui/core';
import axios from 'axios';
var activo=0;
var idRestaurante;
const BASE_URL = 'http://35.222.169.75'
const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

  const Toast = Swal.mixin({
    toast: true,
    position: "top",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });
  

const DialogContent = withStyles((theme) => ({
root: {
    padding: theme.spacing(2),
},
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
root: {
    margin: 0,
    padding: theme.spacing(1),
},
}))(MuiDialogActions);


var restaurante=[]
class Register extends React.Component{
    constructor(props){
        super(props)
        this.state={
            open:false,
            error:false,
            mensaje:false,
       
            alerta:false,
            vacio:false,
        }

      
        // All refs
        this.txtName = React.createRef()
        this.txtApellido = React.createRef()
        this.txtPassword = React.createRef()
        this.txtAddress = React.createRef()
        this.txtEmail = React.createRef()
        // Eventos
        this.handleClickOpen = this.handleClickOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.func_Registro = this.func_Registro.bind(this)
        this.func_obtenerproductos=this.funcllenarselect.bind(this)

    }
    //------API REGISTRARME
       // Interact with APIS



       func_Registro(){
      


        if(this.txtName.current.value=="" || this.txtPassword.current.value=="" || this.txtAddress.current.value=="" || this.txtApellido.current.value=="" || this.txtEmail.current.value==""   ){
          
         this.setState({vacio:true})
         setTimeout(() => {
            this.setState({vacio:false})
            
         }, 3000); 

           return;
          }
        // console.log(this.txtLogInMail.current.value )
        // console.log(this.txtLogInPassword.current.value)
var tipo;
var restaurante;
if(document.getElementById("opcionadmin").checked) {
    tipo=0;
    restaurante=idRestaurante;
}else {
tipo=1;
restaurante=null
}
        var user = {
            name: this.txtName.current.value,
            last_name: this.txtApellido.current.value,
            password: this.txtPassword.current.value,
            address:this.txtAddress.current.value,
            mail: this.txtEmail.current.value,
            id_avatar: this.txtName.current.value+"_"+ this.txtApellido.current.value,
            type : tipo,
            id_restaurante:restaurante
            
        };
      
      
        axios.post(BASE_URL+':4000/py1/user/', user)
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.data.ok){
                console.log("SE REGISTRO CORRECTAMENTE")
                this.txtName.current.value="";
                this.txtApellido.current.value="";
                this.txtAddress.current.value="";
                this.txtEmail.current.value="";
                this.txtPassword.current.value="";
                this.setState({mensaje:true})
                if(this.mensaje==undefined){
                    this.mensaje=true;
                }
                console.log("SI MANDE EL STATE y es "+this.mensaje)
         setTimeout(() => {
            this.setState({mensaje:false})
            this.handleClose();
         }, 2000); 

         
            }else if(res.data.status==409){
                console.log("YA EXISTE UN USUARIO CON ESE CORREO")
                if(this.mensaje==undefined){
                    this.mensaje=true;
                }
                this.setState({alerta:true})
                setTimeout(() => {
                   this.setState({alerta:false})
                 
                }, 2000); 
                 
               // alert("Ya existe un usuario con ese correo registrado")
                this.txtEmail.current.value="";
            }else if(res.data.status==500){
                console.log("DIO ESTATUS 500")
                this.setState({error:true})
                  if(this.mensaje==undefined){
                    this.mensaje=true;
                }
         setTimeout(() => {
            this.setState({error:false})
            this.handleClose();
         }, 2500); 
               
               
            }
           

        })
    }
//---ver como manejar el switch
    funcllenarselect(){

      if(document.getElementById("opcionadmin").checked) {

        var l=document.getElementById("opcionrestaurante");

        l.disabled=false;
       //   alert("SE SELECCIONO ADMIN")
          axios.get(BASE_URL+':4010/py1/restaurante/')
          .then(res => {
              console.log(res);
              console.log(res.data.payload[0]);
              if(res.data.ok){
                  console.log("LA DATA ES "+res.data.payload[0])
                  restaurante=res.data.payload;
  
                  console.log("LA LONGITUD ES "+restaurante.length)
                  var sele=document.getElementById("opcionrestaurante")
                  for(let i=0;i<restaurante.length;i++){
                      console.log("entre al for "+restaurante[i].nombreRestaurante)
                      const option=document.createElement('option');
                      option.value=restaurante[i].idRestaurante;
                      option.text=restaurante[i].nombreRestaurante;
                      sele.appendChild(option)
                  }
                
              }
             
  
          })


          l.addEventListener('change',
  function(){
    var selectedOption = this.options[l.selectedIndex];
    idRestaurante=selectedOption.value;
  //  alert(selectedOption.value + ': ' + selectedOption.text);
  });
      }else {
        var l=document.getElementById("opcionrestaurante");
        l.innerHTML="";
        l.disabled=true;
        l.text="";
      }

     
    }
    func_obtenerproductos(){
        axios.get(BASE_URL+':4000/py1/restaurante/')
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.data.ok){
                console.log("LA DATA ES "+res.data)
              
         
            }
           

        })
    }

    handleClickOpen(){
        this.setState({open:true});
    };
    handleClose(){
        this.setState({open:false});
    };

    
    render(){

        return (



            <div>
                <Button  fullWidth
                                                variant="contained" color="primary" onClick={this.handleClickOpen}>
                    Registro
                </Button>
                <Dialog 
                fullWidth={true}
                onClose={this.handleClose} aria-labelledby="customized-dialog-title" 
                open={this.state.open}>
                    <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
                        Registro
                    </DialogTitle>
                    <DialogContent dividers>
                    <Grid
                            container 
                            spacing={3}
                            direction="column"
                            justify="center"
                            alignItems="stretch"
                        >
                    {this.state.error && (
              <Alert variant="filled" severity="error">
               Ocurrio un error en el servidor
              </Alert>
            )}

{this.state.mensaje && (
   
              <Alert variant="filled" severity="success">
               Usuario creado correctamente
              </Alert>
                
            )}
            
{this.state.alerta && (
   
   <Alert variant="filled" severity="warning">
    Ya existe un usuario registrado con ese correo
   </Alert>
     
 )}

{this.state.vacio && (
   
   <Alert variant="filled" severity="warning">
    Necesita llenar todos los campos del formulario
   </Alert>
     
 )}
             </Grid>

             
             <br></br>
                        <Grid
                            container 
                            spacing={3}
                            direction="column"
                            justify="center"
                            alignItems="stretch"
                        >
                            <Grid item >
                                <TextField 
                                    id="txtName"
                                    inputRef={this.txtName} 
                                    fullWidth
                                    required
                                    label="Nombre" variant="outlined" />
                            </Grid>
                            <Grid item >
                                <TextField 
                                    id="txtApellido"
                                    inputRef={this.txtApellido} 
                                    required
                                    fullWidth
                                    label="Apellido" variant="outlined" />
                            </Grid>
                            <Grid item >
                                <TextField 
                                    id="txtPassword"
                                    inputRef={this.txtPassword}
                                    required
                                    fullWidth
                                    label="Password" type="password" variant="outlined" 
                                />
                            </Grid>
                            <Grid item >
                                <TextField 
                                    id="txtAddress"
                                    inputRef={this.txtAddress} 
                                    required
                                    fullWidth
                                    label="Direccion" variant="outlined" />
                            </Grid>
                            <Grid item >
                                <TextField 
                                    id="txtEmail"
                                    inputRef={this.txtEmail} 
                                    required
                                    fullWidth
                                    label="Email" variant="outlined" />
                            </Grid>

                            <Grid item >
                            <FormControlLabel
    control={<Switch id="opcionadmin" size="small" onChange={this.funcllenarselect} />}
    label="Administrador"
  />
                            </Grid>
                      

                                         
                                            <select
                       id="opcionrestaurante"
                      type="text"
                      disabled
                      fullWidth variant="contained" color="primary" component="span" 
                       placeholder="Restaurante"
                      
                    >
               


       </select>
    
                        </Grid>
              
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button autoFocus onClick={this.func_Registro} variant="contained" color="primary">
                            Registrarme
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
    
    );
    }
}

export default Register
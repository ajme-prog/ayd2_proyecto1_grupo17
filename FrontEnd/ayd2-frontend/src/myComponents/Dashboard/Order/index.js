import React from 'react';
import {useElements} from './state'
import ConfirmDialog from '../../controls/ConfirmDialog';
import Notification from '../../controls/Notification'

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import StepConnector from '@material-ui/core/StepConnector';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import MUIDataTable from "mui-datatables";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Hidden from '@material-ui/core/Hidden';


/* icons */
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import DeleteIcon from '@material-ui/icons/Delete';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import PostAddIcon from '@material-ui/icons/PostAdd';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';

const Orders = ({BASE_URL}) => {
    const {orders, displayorders, 
        cancelOrder, deleteOrder, changeOrderState,
        notification, confirmDialogState} = useElements({BASE_URL})

    const columns = [ 'Registro','Fecha','Nombre del cliente','Direccion','Tipo','Total (Q)']
    const options = {
        filter: true,
        filterType: 'dropdown',
        responsive: 'standard',
        expandableRows: true,
        expandableRowsHeader: false,
        selectableRows: false,
        renderExpandableRow: (rowData, rowMeta) => {
            const colSpan = rowData.length + 1;
            const order = orders[rowMeta.rowIndex]
            const estado = parseInt(order.estado_orden)
            return(
            <DetalleOrden colSpan={colSpan} 
                cancelOrder={cancelOrder}
                deleteOrder={deleteOrder}
                estado={estado <= 3 ? estado: -1}
                tipo_orden={order.tipo_orden}
                partialkey={order.id_orden} 
                detalle={order.productos}
                changeOrderState={changeOrderState}
                setConfirmDialog={confirmDialogState.setConfirmDialog}/>)
        },
        /*onRowExpansionChange: (curExpanded, allExpanded, rowsExpanded) => console.log(curExpanded, allExpanded, rowsExpanded)*/
    };

    return(
        <Card>
            <CardContent>
                <Grid
                    container
                    spacing={2}
                    direction="column"
                    justify="flex-start"
                    alignItems="stretch">
                    <Notification 
                        notify={notification.notify}
                        setNotify={notification.setNotify}/>
                    <ConfirmDialog 
                        confirmDialog={confirmDialogState.confirmDialog}
                        setConfirmDialog={confirmDialogState.setConfirmDialog}/>
                    <Grid 
                    item 
                    md={12}>
                    <MUIDataTable
                        title={"Mis Ordenes"}
                        data={displayorders}
                        columns={columns}
                        options={options}/>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>    
    )
}

const DetalleOrden= ({colSpan, cancelOrder, deleteOrder, changeOrderState,
    estado,tipo_orden, partialkey, detalle, setConfirmDialog}) => {
    return(<TableRow >
        <TableCell colSpan={colSpan}>
            <Grid 
                container 
                spacing={10}>
                <Grid item xs={12} md={6}>
                {/* detail*/}
                {detalle.map((producto, index) => {
                    return(<div key={partialkey + index}>
                    <Typography variant="h5">
                            <label>
                            {producto.nombre_producto}
                            </label>
                        </Typography>
                        <Typography variant="h6">
                            <label>
                            Q.{producto.precio.toFixed(2)}
                            </label>
                        </Typography>
                        <Typography variant="h6">
                            <label>
                            {producto.cant_producto} unidad(es)
                            </label>
                        </Typography>
                        <Typography variant="caption" display="block" gutterBottom>
                            Subtotal: Q.{(producto.precio*producto.cant_producto).toFixed(2)} 
                        </Typography> 
                    </div>)
                })}
                </Grid>
                <Grid item xs={12} md={4}>
                {/* state */}
                <Hidden xsDown>
                    <Grid
                        container
                        spacing={2}
                        justify="center"
                        alignItems="flex-start">
                        <Grid
                            item 
                            xs={12}>
                            {tipo_orden === 0 ? 
                                <Stepper 
                                orientation={'horizontal'}
                                alternativeLabel activeStep={estado} connector={<ColorlibConnector/>}>
                                    {stepsDomicilio.map( label => (
                                        <Step key={label}>
                                            <StepLabel StepIconComponent={ColorlibStepIconDomicilio}>{label}</StepLabel>
                                        </Step>
                                    ))}
                                </Stepper>
                            :
                                <Stepper
                                orientation={'horizontal'}
                                alternativeLabel activeStep={estado} connector={<ColorlibConnector/>}>
                                    {stepsLocal.map( label => (
                                        <Step key={label}>
                                            <StepLabel StepIconComponent={ColorlibStepIconLocal}>{label}</StepLabel>
                                    </Step>
                                    ))}
                                </Stepper>
                            }
                        </Grid>
                        <Grid
                            item
                            xs={6}>
                                <Button
                                    disabled={(estado===0||estado===3 || estado === -1)?true:false}
                                    onClick={()=>{changeOrderState(partialkey,estado - 1)}}
                                    fullWidth variant="contained" color="primary" component="span" startIcon={<ArrowBackIosIcon />}>
                                    Anterior
                                </Button>
                        </Grid>
                        <Grid
                            item
                            xs={6}>
                                <Button
                                    disabled={(estado===3||estado ===-1)?true:false} 
                                    onClick={()=>{changeOrderState(partialkey,estado + 1)}}
                                    fullWidth variant="contained" color="primary" component="span" endIcon={<ArrowForwardIosIcon />}>
                                        Siguiente
                                </Button>
                        </Grid>
                        <Grid
                            item
                            xs={12}>
                            <Button 
                                disabled={(estado !== -1 && estado !== 3)? true : false}
                                onClick={()=>{
                                    setConfirmDialog({
                                        isOpen:true,
                                        title:'Are you sure you want to delete the order?',
                                        subtitle:'You can\'t undo this operation',
                                        onConfirm: () => deleteOrder(partialkey)
                                    })
                                }}
                                fullWidth variant="contained" component="span" endIcon={<DeleteIcon />}>
                                    Eliminar orden
                            </Button>
                        </Grid>
                        <Grid
                            item
                            xs={12}>
                            <Button 
                                disabled={estado !== 0 ? true : false}
                                onClick={()=>{
                                    setConfirmDialog({
                                        isOpen:true,
                                        title:'¿Esta seguro de eliminar esta orden?',
                                        subtitle:'Esta operación no se puede deshacer',
                                        onConfirm: () => cancelOrder(partialkey)
                                    })
                                }}
                                fullWidth variant="contained" color="primary" component="span" endIcon={<SentimentDissatisfiedIcon />}>
                                    Cancelar orden
                            </Button>
                        </Grid>
                    </Grid>
                            
                </Hidden>
    
                <Hidden smUp>
                    <Grid
                        container
                        spacing={2}
                        justify="flex-start"
                        alignItems="flex-start">
                        <Grid
                            item 
                            xs={6}>
                            {tipo_orden === 0 ? 
                                <Stepper 
                                orientation={'vertical'}
                                alternativeLabel activeStep={estado} >
                                    {stepsDomicilio.map( label => (
                                        <Step key={label}>
                                            <StepLabel StepIconComponent={ColorlibStepIconDomicilio}>{label}</StepLabel>
                                        </Step>
                                    ))}
                                </Stepper>
                            :
                                <Stepper 
                                orientation={'vertical'}
                                alternativeLabel activeStep={estado} >
                                    {stepsLocal.map( label => (
                                        <Step key={label}>
                                            <StepLabel StepIconComponent={ColorlibStepIconLocal}>{label}</StepLabel>
                                    </Step>
                                    ))}
                                </Stepper>
                            }
                        </Grid>
                        <Grid
                            item
                            xs={12} md={6}>

                        <Grid
                        container
                        direction="column"
                        spacing={2}
                        justify="flex-start"
                        alignItems="flex-start">
                        <Grid
                            item
                            xs={4} md={6}>
                                <Button
                                    disabled={(estado===0||estado===3 || estado === -1)?true:false}
                                    onClick={()=>{changeOrderState(partialkey,estado - 1)}}
                                    fullWidth variant="contained" color="primary" component="span" startIcon={<ArrowBackIosIcon />}>
                                    Anterior
                                </Button>
                        </Grid>
                        <Grid
                            item
                            xs={4} md={6}>
                                <Button
                                    disabled={(estado===3||estado ===-1)?true:false} 
                                    onClick={()=>{changeOrderState(partialkey,estado + 1)}}
                                    fullWidth variant="contained" color="primary" component="span" endIcon={<ArrowForwardIosIcon />}>
                                        Siguiente
                                </Button>
                        </Grid>
                        <Grid
                            item
                            xs={12}>
                            <Button 
                                disabled={(estado !== -1 && estado !== 3)? true : false}
                                onClick={()=>{
                                    setConfirmDialog({
                                        isOpen:true,
                                        title:'¿Esta seguro de eliminar este producto?',
                                        subtitle:'Esta operación no se puede deshacer',
                                        onConfirm: () => deleteOrder(partialkey)
                                    })
                                }}
                                fullWidth variant="contained" component="span" endIcon={<DeleteIcon />}>
                                    Eliminar orden
                            </Button>
                        </Grid>
                        <Grid
                            item
                            xs={12}>
                            <Button 
                                disabled={estado !== 0 ? true : false}
                                onClick={()=>{
                                    setConfirmDialog({
                                        isOpen:true,
                                        title:'¿Esta seguro de eliminar este producto?',
                                        subtitle:'Esta operación no se puede deshacer',
                                        onConfirm: () => cancelOrder(partialkey)
                                    })
                                }}
                                fullWidth variant="contained" color="primary" component="span" endIcon={<SentimentDissatisfiedIcon />}>
                                    Cancelar orden
                            </Button>
                        </Grid>

                        </Grid>
                        </Grid>
                        </Grid>
                            
                </Hidden>
    
                </Grid>
            </Grid>
        </TableCell>
    </TableRow>)
}

const stepsDomicilio = ['Nueva orden', 'En preparacion','En camino','Pagada'];
const stepsLocal = ['Nueva orden', 'En preparacion','Pagada'];

function ColorlibStepIconDomicilio(props) {
    const classes = useColorlibStepIconStyles();
    const { active, completed } = props;
  
    const iconsDomicilio = {
        1: <PostAddIcon />,
        2: <ShoppingBasketIcon />,
        3: <LocalShippingIcon />,
        4: <SentimentVerySatisfiedIcon />,
    };
    return (
        <div className={clsx(classes.root, {
          [classes.active]: active,
          [classes.completed]: completed,})}>
          {iconsDomicilio[String(props.icon)]}
        </div>
    );
}

function ColorlibStepIconLocal(props) {
    const classes = useColorlibStepIconStyles();
    const { active, completed } = props;
  
    const iconsLocal = {
      1: <PostAddIcon />,
      2: <ShoppingBasketIcon />,
      3: <SentimentVerySatisfiedIcon />,
    };
    return (
      <div className={clsx(classes.root, {
          [classes.active]: active,
          [classes.completed]: completed,})}>
          {iconsLocal[String(props.icon)]}
      </div>
    );
}

const ColorlibConnector = withStyles({
    alternativeLabel: {
        top: 22,
    },
    active: {
        '& $line': {
            backgroundImage:
            'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    completed: {
        '& $line': {
            backgroundImage:
            'linear-gradient( 95deg,rgb(242,113,33) 0%,rgb(233,64,87) 50%,rgb(138,35,135) 100%)',
        },
    },
    line: {
        height: 3,
        border: 0,
        backgroundColor: '#eaeaf0',
        borderRadius: 1,
    },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
    root: {
        backgroundColor: '#ccc',
        zIndex: 1,
        color: '#fff',
        width: 50,
        height: 50,
        display: 'flex',
        borderRadius: '50%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    active: {
        backgroundImage:
            'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(242,113,33) 50%, rgb(242,113,33) 100%)',
        boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
    },
    completed: {
        backgroundImage:
        'linear-gradient( 136deg, rgb(242,113,33) 0%, rgb(242,113,33) 50%, rgb(242,113,33) 100%)',
    },
});

export default Orders
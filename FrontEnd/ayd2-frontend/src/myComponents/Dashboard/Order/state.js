import React, { useEffect, useState } from 'react';
const crypto = require('crypto');

export function useElements({BASE_URL}){
    
  const [usr,] = useState(JSON.parse(localStorage.getItem('usr')))
  const [orders,setOrders] = useState([])
  const [displayorders,setDisplayOrders] = useState([])
  const [notify,setNotify] = useState({isOpen:false, message: '', type:'' })
  const [confirmDialog,setConfirmDialog] = useState({isOpen:false,title:'',subtitle:''})


  const getOrdenes = () =>{
    var idRestaurante = JSON.parse(localStorage.getItem('usr')).idRestaurante
    fetch(BASE_URL+':3030/orders/ordenes-restaurante/?id_restaurante='+idRestaurante+"")
      .then(res => res.json())
      .then(json => {
        setOrders([...calculateTotal(json)])
        const aux = []
        json.forEach(order => {
          const date = new Date(order.fecha);
          const datefmt = date.getDate()+"/"+(parseInt(date.getMonth())+1)+"/"+date.getFullYear()+" - "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()
          aux.push([
              order.id_orden,
              datefmt,
              order.nombre_usuario + ' ' + order.apellido_usuario,
              decrypt(order.direccion), 
              order.tipo_orden === 0? 'A domiclio':'Recoger en tienda',
              order.total ])
        })
        setDisplayOrders([...aux])
      })
  }

  const changeOrderState = (id_orden, newestado) => {
    fetch(BASE_URL+':3000/orders/actualizar-estado-orden',{
      method:'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id_orden,
        id_usuario:usr.idUsuario,
        estado: newestado
        })
      })
      .then(res => res.json())
      .then(json => {
        if(json.status === 200){
          getOrdenes()
          setNotify({isOpen:true,message:'Orden modificada con exito',type:'success'})
          return
        }
        setNotify({isOpen:true,message:'No actualizar el estado de la orden',type:'error'})
      }).catch(err => {
        setNotify({isOpen:true,message:'Erro al actualizar el estado de la orden',type:'error'})
      })
  }

  const deleteOrder = (id_orden) => {
    setConfirmDialog({...confirmDialog,isOpen:false})
    fetch(BASE_URL+':3020/orders/eliminar-orden?id_usuario='+usr.idUsuario+'&id_orden='+id_orden,{
      method:'DELETE'})
      .then(res => res.json())
      .then(json => {
        if(json.status === 200){
          getOrdenes()
          setNotify({isOpen:true,message:'Orden eliminada con exito',type:'success'})
        } 
      })
  } 

  const cancelOrder = (id_orden) => {
    setConfirmDialog({...confirmDialog,isOpen:false})
    fetch(BASE_URL+':3000/orders/actualizar-estado-orden',{
      method:'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id_orden,
        id_usuario:usr.idUsuario,
        estado:"4"
        })
      })
      .then(res => res.json())
      .then(json => {
        if(json.status === 200){
          getOrdenes()
          setNotify({isOpen:true,message:'Orden cancelada con exito',type:'success'})
          return
        }
        setNotify({isOpen:true,message:'No se pudo cancelar la orden',type:'error'})
      })
  } 

  useEffect(()=>{
    if(usr!=null) getOrdenes()
  },[])

  return {
    orders,
    displayorders,
    cancelOrder,
    deleteOrder,
    changeOrderState,
    notification:{
      notify,
      setNotify
    },
    confirmDialogState:{
      confirmDialog,
      setConfirmDialog
    }
  }
}

function decrypt(text) {
  const algorithm = 'aes-256-ctr';
  const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  //console.log(decrypted.toString())
  return decrypted.toString();
}

function calculateTotal(orders){
  const aux = []
  let total = 0
  orders.forEach((order) => {
    total = 0
    order.productos.forEach(product => {
      total += product.cant_producto * product.precio
    });
    order.total = total.toFixed(2)
    aux.push(order)
  })
  return aux
}
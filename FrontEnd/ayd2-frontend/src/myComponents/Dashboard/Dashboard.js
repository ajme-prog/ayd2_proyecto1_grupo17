import React from 'react';
// My components
import ItemInShoppingCart from './ItemInShoppingCart'
import Purchase from './Purchase'
import YourOrders from './YourOrder'
import Orders from './Order'

import CRUDProducts from './CRUDProducts'


// Material react components
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import FastfoodIcon from '@material-ui/icons/Fastfood';

import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import MenuIcon from '@material-ui/icons/Menu';
import AddCircleOutlineTwoToneIcon from '@material-ui/icons/AddCircleOutlineTwoTone';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import ForwardIcon from '@material-ui/icons/Forward';
import ShoppingCartTwoToneIcon from '@material-ui/icons/ShoppingCartTwoTone';
import RemoveCircleOutlineTwoToneIcon from '@material-ui/icons/RemoveCircleOutlineTwoTone';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepButton from '@material-ui/core/StepButton';
import Hidden from '@material-ui/core/Hidden';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import InfoIcon from '@material-ui/icons/Info';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import TocIcon from '@material-ui/icons/Toc';

import Pagination from '@material-ui/lab/Pagination';
import Collapse from '@material-ui/core/Collapse';
import FilterListIcon from '@material-ui/icons/FilterList';
import { withStyles } from '@material-ui/core';
import Swal from "sweetalert2";
import Register from './Register'
import RegistroRestaurante from './RegistroRestaurante'

import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '@material-ui/lab/Alert';

import Notification from './Notification2'

import LoyaltyIcon from '@material-ui/icons/Loyalty';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import CardMedia from '@material-ui/core/CardMedia';

// const BASE_URL = "http://"+process.env.REACT_APP_HOST_API
const BASE_URL = 'http://35.222.169.75'
const img_BASE_URL_S3 = 'https://productos-ayd2.s3.us-east-2.amazonaws.com/'

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            {children}
          </Box>
        )}
      </div>
    );
  }

class Dashboard extends React.Component{
    constructor(props){
        super(props)
        this.state={
            shoppingCarContent:[
                // {
                //     name:'Ramen',
                //     quantity:3,
                //     price:10.50,
                //     img:'https://upload.wikimedia.org/wikipedia/commons/d/dc/Shoyu_Ramen.jpg'
                // },
                // {
                //     name:'Pizza',
                //     quantity:2,
                //     price:10.50,
                //     img:'https://www.guatemala.com/fotos/2020/07/Pizza-a-domicilio.jpg'
                // },
            ],
            shoppingCarContentByRestaurant:{},
            shoppingCarTotal:0,
            shoppingCarTabPanelIndex:0,
            completedSteps:[
                false,
                false,
                false
            ],
            activeStep:0,
            showMoreFilters:false,
            productShowOnlyOffers:false,
            toogle_left:false,
            toogle_right:false,
            tabIndex:0,
            tabIndexRight:0,
            switch_registrar:false,
            steps :['Elegir', 'Comprar', 'Historial'],
            suggestionList:[
                'Un rico estofado',
                'Una sopa caliente',
                'Un sabroso pollo asado',
                'Unas papas ricolinas',
            ],
            originalTileData:[],
            tileData:[],
            paginationIndex:0,
            paginationSize:9,
            paginationLenght:0,
            toggle_shopping_cart:false,
            
            // User interface control
            isAdminUser:false,
            isLoggedUser:false,
            // notificationProps
            notiIsOpen:false,
            notiType:"info",
            notiMessage:"Mensaje"
        }
        // Methods binded
        this.onChangeMoreFilters = this.onChangeMoreFilters.bind(this)
        this.func_logIn = this.func_logIn.bind(this)
        this.showMenuIfIsAdmin = this.showMenuIfIsAdmin.bind(this)
        this.func_getAllProducts = this.func_getAllProducts.bind(this)
        this.handleQuantityChange = this.handleQuantityChange.bind(this)
        this.handleQuantityChangeOne = this.handleQuantityChangeOne.bind(this)
        this.handleAddToCart = this.handleAddToCart.bind(this)
        this.handleChangeTxtBuscar = this.handleChangeTxtBuscar.bind(this)
        this.handleChangePagination = this.handleChangePagination.bind(this)

        this.handleCarContentQuantityChange = this.handleCarContentQuantityChange.bind(this)
        this.handleCarContentQuantityChangeOne = this.handleCarContentQuantityChangeOne.bind(this)
        this.handleCarContentQuantityChangeRemove = this.handleCarContentQuantityChangeRemove.bind(this)
        this.nextStep = this.nextStep.bind(this)
        this.updatePaginationLenght = this.updatePaginationLenght.bind(this)
        this.displayNotification = this.displayNotification.bind(this)

        this.productInfoModalShow = this.productInfoModalShow.bind(this)
        this.onChangeProductShowOnlyOffers=this.onChangeProductShowOnlyOffers.bind(this)
        // All refs
        this.txtLogInMail = React.createRef()
        this.txtLogInPassword = React.createRef()
        this.txtBuscarProducto = React.createRef()
        
    }
    // React - Cycle of life
    componentDidMount(){
        this.showMenuIfIsAdmin()
        this.func_getAllProducts()
    }
    // Interact with APIS
    func_logIn(){
        if(this.state.isLoggedUser){
            //Entonces quiere salir
            localStorage.removeItem('usr')
            localStorage.removeItem('tkn')
            this.displayNotification('success',"Que tengas un gran dia!, nos vemos pronto!")
            this.setState({
                isAdminUser:false,
                isLoggedUser:false,
                tabIndexRight:0
            })

            this.func_getAllProducts()
            return
        }
        // console.log(this.txtLogInMail.current.value )
        // console.log(this.txtLogInPassword.current.value)
        var user = {
            mail: this.txtLogInMail.current.value,
            password: this.txtLogInPassword.current.value
        };
      
        axios.post(BASE_URL+':4000/py1/login', user)
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.data.ok){
                localStorage.setItem('tkn',res.data.payload.token)
                //console.log('El usuario existe')
                //console.log('ID '+res.data.payload.id)
                this.displayNotification('success',"Bienvenido")
                this.func_getUser(res.data.payload.id)
            }else{
                this.displayNotification('info',"Credenciales invalidas")

            }
        })

        this.func_getAllProducts()
    }
    func_getUser(id){
        console.log(localStorage.getItem('tkn'))
        axios.get(BASE_URL+':4000/py1/user/'+id,
            {
                headers: {
                    Authorization: 'Bearer '+localStorage.getItem('tkn')
                }
            }
        )
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.status==200){
                console.log("Esto se va a guardar")
                console.log(res.data.payload)
                var jsonUsr = JSON.stringify(res.data.payload)
                localStorage.setItem('usr',jsonUsr)
                this.showMenuIfIsAdmin()
                this.displayNotification('success',"Bienvenido")
            }else{
                this.displayNotification('error',"Sesion finalizada")
            }
        })
    }
    func_getAllProducts(){
        console.log(BASE_URL)
        console.log('CRUDProducts - Obtener todos los productos')
        axios.get(BASE_URL+':4010/py1/product')
        .then(res=>{
            console.log(res)
            console.log(res.data)
            var arr = []
            var arr_from_db = res.data.payload
            arr_from_db.forEach(prod=>{
                var index = prod.idProducto
                arr.push(
                    {
                            "id":prod.idProducto,
                            "name":prod.nombreProducto,
                            "price":prod.precio,
                            "img":prod.imagen,
                            "quantity":1,
                            "offer":prod.oferta,
                            "Restaurante_idRestaurante":prod.Restaurante_idRestaurante
                    }
                )
            })
            console.log("FINALMENTE")
            console.log(arr)
            this.updatePaginationLenght(arr.length)
            //Tamanio de paginacion
            this.setState({
                tileData:arr,
                originalTileData:arr,
            })
        })
    }
    updatePaginationLenght(len){
        this.state.paginationLenght = Math.ceil(len/this.state.paginationSize)
        this.setState({
            paginationLenght:this.state.paginationLenght
        })
    }
    // Utilities
    productInfoModalShow(prodId){
        console.log("si llego")
        console.log(prodId)

    }
    handleQuantityChange(e){
        if(isNaN(e.target.value)){
            return
        }
        this.state.tileData[e.target.id].quantity=e.target.value
        this.setState({tileData:this.state.tileData})
    }
    handleQuantityChangeOne(id,cuantity){
        if(this.state.tileData[id].quantity<=0){
            if(cuantity<=-1){
                return
            }
    
        }
        this.state.tileData[id].quantity=this.state.tileData[id].quantity+cuantity
        this.setState({tileData:this.state.tileData})
    }
    handleAddToCart(id,index){
        var alreadyInTheCar = false
        var indexIntoShoppingCart = 0
        this.state.shoppingCarContent.forEach(prod=>{
            if(prod.id==id){
                this.state.shoppingCarContent[indexIntoShoppingCart].quantity=this.state.tileData[index].quantity
                this.setState({
                    shoppingCarContent:this.state.shoppingCarContent
                })
                console.log('agrego')
//                this.state.shoppingCarContentByRestaurant[prod.Restaurante_idRestaurante].quantity = this.state.tileData[index].quantity
        
                alreadyInTheCar = true
                return
            }
            indexIntoShoppingCart++
        })
        if(alreadyInTheCar){
            return
        }
        var tempPrice=this.state.tileData[index].offer
        
        if(tempPrice==0){
            tempPrice = this.state.tileData[index].price
        }
        axios.get(BASE_URL+':4010/py1/restaurante/'+this.state.tileData[index].Restaurante_idRestaurante)
        .then(
            res=>{
                const infoRes = res.data.payload[0];
                var newProdToCart={
                    id:id,
                    name:this.state.tileData[index].name,
                    price:tempPrice,
                    img:img_BASE_URL_S3+this.state.tileData[index].img,
                    quantity:this.state.tileData[index].quantity,
                    Restaurante_idRestaurante:this.state.tileData[index].Restaurante_idRestaurante,
                    nombreRestaurante:infoRes.nombreRestaurante,
                    logo:img_BASE_URL_S3+infoRes.logo
                }
                if(!(infoRes.idRestaurante in this.state.shoppingCarContentByRestaurant)){
                    this.state.shoppingCarContentByRestaurant[infoRes.idRestaurante]=[]
                }
                this.state.shoppingCarContentByRestaurant[infoRes.idRestaurante].push(
                    newProdToCart
                )
                this.state.shoppingCarContent.push(
                    newProdToCart
                )
                this.setState({
                    shoppingCarContentByRestaurant:this.state.shoppingCarContentByRestaurant,
                    shoppingCarContent:this.state.shoppingCarContent
                })
                
            }
        );

    }
    showMenuIfIsAdmin(){
        if(localStorage.getItem('usr')==null){
            return
        }
        var actualUser=JSON.parse(localStorage.getItem('usr'))
        console.log(actualUser)
        if(localStorage.getItem('tkn')){
            if(actualUser!=null){
                console.log(actualUser.tipoUsuario)
                if(actualUser.tipoUsuario==0){
                    this.setState({
                        isAdminUser:true,
                        logoRestaurante:actualUser.logoRestaurante,
                        nombreRestaurante:actualUser.nombreRestaurante,
                        isLoggedUser:true,
                        tabIndexRight:3
                    })
                }else{
                    this.setState({
                        isLoggedUser:true,
                        tabIndexRight:0
                    })
                    this.func_getAllProducts()
                }
            }else{
                this.setState({
                    isAdminUser:false,
                    isLoggedUser:true,
                    tabIndexRight:0
                })
                this.func_getAllProducts()
            }
        }else{
            
        }
    }
    handleStep = (step) => () => {
        //console.log(step)
        var i;
        for(i=0;i<3;i++){
            if(i<step){
                this.state.completedSteps[i]=true;
            }else{
                this.state.completedSteps[i]=false;                
            }
        }
        this.setState({
            activeStep:step,
            tabIndexRight:step,
            completedSteps:this.state.completedSteps
        });

    }
    onChangeMoreFilters(){
        this.setState({showMoreFilters:!this.state.showMoreFilters})
    }
    onChangeProductShowOnlyOffers(){
        this.setState({productShowOnlyOffers :!this.state.productShowOnlyOffers})
        this.handleProductShowOnlyOffers()
    }
    toggleDrawerLeft = (anchor, open) => (event) => {
        this.setState({  toogle_left:!this.state.toogle_left });
    };
    toggleDrawerRight = (anchor, open) => (event) => {
        this.setState({toogle_right:!this.state.toogle_right});
    };
    toggleDrawerShoppginCart = (anchor, open) => (event) => {
        this.func_toggleDrawerShoppingCart(!this.state.toggle_shopping_cart)
    };
    func_toggleDrawerShoppingCart(val){
        this.setState({toggle_shopping_cart:val});
    }
    tabOnChange = (event, newValue) => {
        this.setState({tabIndex:newValue})
    }
    tabOnChangeRight = (event, newValue) => {
        this.func_tabOnChangeRight(newValue)
    }
    func_tabOnChangeRight(val){
        this.setState({tabIndexRight:val})
    }
    handleChangeTxtBuscar(e){
        var newTileData = []
        this.state.tileData=this.state.originalTileData
        if(e.target.value.trim()===""){
            this.setState({tileData:this.state.tileData})
            return
        }
        console.log('tileData')
        console.log(this.state.tileData)
        this.state.tileData.forEach(element=>{
            console.log('prod')
            console.log(element)
            console.log(element.name)
            if (element.name.toLowerCase().includes(e.target.value.toLowerCase())){
                newTileData.push(element)
            }
        })
        this.setState({tileData:newTileData})
        this.updatePaginationLenght(newTileData.length)
    }
    handleProductShowOnlyOffers(){
        var newTileData = []
        if(this.state.productShowOnlyOffers){
            this.state.tileData=this.state.originalTileData
            this.setState({tileData:this.state.tileData})
            return
        }
        this.state.tileData=this.state.originalTileData
        console.log('tileData')
        console.log(this.state.tileData)
        this.state.tileData.forEach(element=>{
            console.log('prod')
            console.log(element)
            console.log(element.name)
            if (element.offer!=0){
                newTileData.push(element)
            }
        })
        this.setState({tileData:newTileData})
        this.updatePaginationLenght(newTileData.length)
    }
    handleChangePagination(event,page){
        this.setState({paginationIndex:page-1})
    }
    handleCarContentQuantityChange(e){
        if(isNaN(e.target.value)){
            return
        }
        this.state.shoppingCarContent[e.target.id].quantity=e.target.value
        this.setState({shoppingCarContent:this.state.shoppingCarContent})
    }
    handleCarContentQuantityChangeOne(id,cuantity){
        console.log(id)
        console.log(cuantity)
        if(this.state.shoppingCarContent[id].quantity<=0){
            if(cuantity<=-1){
                return
            }
        }
        this.state.shoppingCarContent[id].quantity=this.state.shoppingCarContent[id].quantity+cuantity
        this.setState({shoppingCarContent:this.state.shoppingCarContent})
    }
    handleCarContentQuantityChangeRemove(indexInArray){
        this.state.shoppingCarContent.splice(indexInArray,1)
        this.setState({shoppingCarContent:this.state.shoppingCarContent})

    }
    nextStep(){
        this.state.completedSteps[1]=true;
        this.state.activeStep=2
        this.setState({
            shoppingCarContent:[],
            shoppingCarTotal:0,
            tabIndexRight:2,
            completedSteps:2,
            completedSteps:this.state.completedSteps
        })
    }
    displayNotification(severity,msj){
        console.log(severity)
        console.log(msj)
        this.state.notiIsOpen=true
        this.state.notiType=severity
        this.state.notiMessage=msj
        this.setState({
            notiIsOpen:true,
            notiType:this.state.notiType,
            notiMessage:this.state.notiMessage
        })
    }
    render(){
        var max = this.state.suggestionList.length - 1
        var suggestionIndex = Math.floor(max - 0) + 0;
        const items = []
        this.state.shoppingCarTotal = 0
        var that = this
        this.state.shoppingCarContent.map(
            (element,index) => {
                console.log(element)
                var subt = element.quantity*element.price
                items.push(
                    <  ItemInShoppingCart 
                    indexInArray={index}
                    idProd={element.id}
                    key={element.id}
                    quantity={element.quantity} 
                    name={element.name} 
                    price={element.price} 
                    subtotal={subt}
                    img={element.img}
                    nombreRestaurante={element.nombreRestaurante}
                    logo={element.logo}

                    // Metodos
                    handleCarContentQuantityChange={this.handleCarContentQuantityChange}
                    handleCarContentQuantityChangeOne={this.handleCarContentQuantityChangeOne}
                    handleCarContentQuantityChangeRemove={this.handleCarContentQuantityChangeRemove}
                    displayNotification={this.displayNotification}
                    />
    
                )
                this.state.shoppingCarTotal+=subt
            }
        ) 
        var a;
        return(
        <div >
            <Notification 
                    isOpen={this.state.notiIsOpen}
                    type={this.state.notiType}
                    message={this.state.notiMessage}
                    
                handleClose={()=>{this.setState({notiIsOpen:false})}}/>
               {/* <ConfirmDialog 
                    confirmDialog={confirmDialogState.confirmDialog}
                    setConfirmDialog={confirmDialogState.setConfirmDialog}/> */}

            <Grid
                container
                spacing={3}
                direction="column"
                justify="center"
                alignItems="stretch"
            >
                <AppBar position="sticky">
                    <Toolbar>
                        <Grid container spacing={2} direction="row" alignItems="center">
                            <Grid item md={1} xs={2}>
                            {this.state.isAdminUser?
                                <IconButton color="inherit" aria-label="menu"
                                onClick={(event)=>{this.setState({toogle_left:true})}}>
                                <MenuIcon/>
                                </IconButton>
                            :null}

            
                            </Grid>
                            <Hidden mdDown>
                            <Grid item md={4} xs={6}>
                                <Grid
                                container
                                spacing={3}
                                >
                                    <Grid
                                    item
                                    >
                                        {this.state.isAdminUser?
                                        <div>
                                            <img
                                            style={{padding:'5px',width:'20px', height:'auto'}}
                                            src={img_BASE_URL_S3+this.state.logoRestaurante}
                                            ></img>
                                        </div>
                                        :null
                                        }
                                    </Grid>
                                    <Grid
                                    item
                                    >
                                        <Typography variant="h6">
                                            {this.state.isAdminUser?
                                            <div>
                                                {this.state.nombreRestaurante}
                                            </div>
                                            :
                                            <div>
                                            Tu carreta online
                                            </div>
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>

                            </Grid>
                            <Grid item md={4} xs={12}>
                                {this.state.isAdminUser?
                                null
                                :
                                <Stepper nonLinear activeStep={this.state.activeStep}>
                                    {this.state.steps.map((label, index) => {
                                    const stepProps = {};
                                    const labelProps = {};
                                    return (
                                        <Step key={index} {...stepProps}>
                                            <StepButton onClick={this.handleStep(index)} completed={this.state.completedSteps[index]}>
                                                {label}
                                            </StepButton>
                                        </Step>
                                    );
                                    })}
                                </Stepper>
                                }
                            </Grid>
                            {this.state.isAdminUser?
                                null   
                                :
                                <IconButton color="inherit" aria-label="carrito"
                                    onClick={(event)=>{
                                        // this.displayNotification('info',"mensajin")
                                        this.setState({toggle_shopping_cart:true})}}
                                    >
                                    <ShoppingCartIcon></ShoppingCartIcon>
                                </IconButton>
                                }

                                <Grid item md={1} xs={2}>
                                    <IconButton 
                                    id={"btn_account_lg"}
                                    color="inherit" aria-label="mi cuenta"
                                    onClick={(event)=>{this.setState({toogle_right:true})}}
                                    >
                                        <AccountCircleIcon></AccountCircleIcon>
                                    </IconButton>
                                </Grid>
                            </Hidden>
                            

                            <Hidden lgUp>
                                <Grid item md={4} xs={12}>
                                {this.state.isAdminUser?
                                null
                                :
                                <Stepper nonLinear activeStep={this.state.activeStep}>
                                    {this.state.steps.map((label, index) => {
                                    const stepProps = {};
                                    const labelProps = {};
                                    return (
                                        <Step key={index} {...stepProps}>
                                            <StepButton onClick={this.handleStep(index)} completed={this.state.completedSteps[index]}>
                                                {label}
                                            </StepButton>
                                        </Step>
                                    );
                                    })}
                                </Stepper>
                                }
                            </Grid>
                            <Grid item md={4} xs={8}>
                            <Grid
                                container
                                spacing={3}
                                >
                                    <Grid
                                    item
                                    >
                                        {this.state.isAdminUser?
                                        <div>
                                            <img
                                            style={{padding:'5px',width:'20px', height:'auto'}}
                                            src={img_BASE_URL_S3+this.state.logoRestaurante}
                                            ></img>
                                        </div>
                                        :null
                                        }
                                    </Grid>
                                    <Grid
                                    item
                                    >
                                        <Typography variant="h6">
                                            {this.state.isAdminUser?
                                            <div>
                                                {this.state.nombreRestaurante}
                                            </div>
                                            :
                                            <div>
                                            Tu carreta online
                                            </div>
                                            }
                                        </Typography>
                                    </Grid>

                                </Grid>
                            </Grid>

                                <Grid item md={1} xs={1}>
                                {this.state.isAdminUser?
                                null   
                                :
                                <IconButton color="inherit" aria-label="mi cuenta"
                                    onClick={(event)=>{this.setState({toggle_shopping_cart:true})}}
                                    >
                                    <ShoppingCartIcon></ShoppingCartIcon>
                                </IconButton>
                                }
                                </Grid>
                                    <Grid item md={1} xs={1}>
                                    <IconButton
                                    id={"btn_account_sm"}
                                    color="inherit" aria-label="mi cuenta"
                                        onClick={(event)=>{this.setState({toogle_right:!this.state.toggle_shopping_cart})}}
                                    >
                                        <AccountCircleIcon></AccountCircleIcon>
                                    </IconButton>

                                </Grid>
                            </Hidden>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <Toolbar />

                {/* <br/>
                <br/>
                <br/>
                <Hidden lgUp>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </Hidden> */}
                <Grid item xs={12}>
                    <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="center"
                    >
                        <Grid item xs={12}>
                            <Grid 
                                container 
                                spacing={0}
                                direction="row"
                                justify="space-between"
                                alignItems="center"
                            >
                                <Grid item 
                                    xs={12}
                                >
                                    <TabPanel value={this.state.tabIndexRight} index={0}>
                                        <TitlebarGridList 
                                        displayNotification={this.displayNotification}
                                        handleQuantityChange={this.handleQuantityChange}
                                        handleQuantityChangeOne={this.handleQuantityChangeOne}
                                        handleAddToCart={this.handleAddToCart}
                                        showMoreFilters={this.state.showMoreFilters} onChangeMoreFilters={this.onChangeMoreFilters} tileData={this.state.tileData}
                                        suggestion={this.state.suggestionList[suggestionIndex]}
                                        handleChangeTxtBuscar={this.handleChangeTxtBuscar}
                                        handleChangePagination={this.handleChangePagination}
                                        paginationIndex={this.state.paginationIndex}
                                        paginationSize={this.state.paginationSize}
                                        paginationLenght={this.state.paginationLenght}
                                        // Product info
                                        productInfoModalShow={this.productInfoModalShow}
                                        onChangeProductShowOnlyOffers={this.onChangeProductShowOnlyOffers}
                                        productShowOnlyOffers={this.state.productShowOnlyOffers}
                                         />
                                    </TabPanel>
                                    <TabPanel value={this.state.tabIndexRight} index={1}>
                                        <Purchase 
                                        displayNotification={this.displayNotification}
                                        nextStep={this.nextStep}
                                        BASE_URL={BASE_URL} 
                                        shoppingCarContent={this.state.shoppingCarContent} 
                                        shoppingCarContentByRestaurant={this.state.shoppingCarContentByRestaurant}
                                        items={items} 
                                        shoppingCarTotal={this.state.shoppingCarTotal}/>
                                    </TabPanel>
                                    <TabPanel value={this.state.tabIndexRight} index={2}>
                                        <YourOrders BASE_URL={BASE_URL} />
                                    </TabPanel>
                                    <TabPanel value={this.state.tabIndexRight} index={3}>
                                        <CRUDProducts
                                        displayNotification={this.displayNotification}
                                        BASE_URL={BASE_URL} />
                                    </TabPanel>
                                    <TabPanel value={this.state.tabIndexRight} index={4}>
                                        <Orders BASE_URL={BASE_URL} />
                                    </TabPanel>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <SwipeableDrawer
                    anchor={'left'}
                    open={this.state.toogle_left}
                    onClose={this.toggleDrawerLeft(false)}
                    onOpen={this.toggleDrawerLeft(true)}
                >
                    <div
                        role="presentation"
                        onClick={()=>{}}
                        onKeyDown={()=>{}}
                        >
                        <List>
                            {/* <ListItem button={true} key={'Productos'}
                                onClick={()=>{this.func_tabOnChangeRight(0)}}
                            >
                                <ListItemIcon><ContactMailIcon /> </ListItemIcon>
                                <ListItemText primary={'Productos'} />
                            </ListItem>
                            <ListItem button={true} key={'Comprar'}
                                onClick={()=>{this.func_tabOnChangeRight(1)}}
                            >
                                <ListItemIcon><ContactMailIcon /> </ListItemIcon>
                                <ListItemText primary={'Comprar'} />
                            </ListItem> 
                        <Divider />*/}
                            <ListItem button={true} key={'Productos'}
                                onClick={()=>{this.func_tabOnChangeRight(3)}}
                            >
                                <ListItemIcon><FastfoodIcon /> </ListItemIcon>
                                <ListItemText primary={'Productos'} />
                            </ListItem>
                            <ListItem button={true} key={'ProcesarOrdenes'}
                                onClick={()=>{this.func_tabOnChangeRight(4)}}
                            >
                                <ListItemIcon><TocIcon /> </ListItemIcon>
                                <ListItemText primary={'Procesar ordenes'} />
                            </ListItem>
                        </List>
                        </div>
                </SwipeableDrawer>
                <SwipeableDrawer
                    anchor={'right'}
                    open={this.state.toogle_right}
                    onClose={this.toggleDrawerRight(false)}
                    onOpen={this.toggleDrawerRight(true)}
                >
                <div
                    role="presentation"
                    onClick={()=>{}}
                    onKeyDown={()=>{}}
                    >
                        <div>
                            <IconButton 
                                onClick={this.toggleDrawerRight()}
                                aria-label="Atras">
                                <ArrowBackIcon/>
                            </IconButton>
                                <Tabs
                                    id={"tab_bienvenido"}
                                    value={this.state.tabIndex}
                                    onChange={this.tabOnChange}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    aria-label="Bienvenido"
                                    centered={true}
                                    fullWidth={true}
                                    >
                                    <Tab 
                                    fullWidth={true}
                                    icon={<AssignmentIndIcon />}
                                    label={this.state.isLoggedUser?"Bienvenido":"Ingresar"}
                                    />
                                </Tabs>
                                <TabPanel value={this.state.tabIndex} index={0}>
                                    <form noValidate autoComplete="off">
                                        <Grid
                                            container 
                                            spacing={3}
                                            direction="column"
                                            justify="center"
                                            alignItems="stretch"
                                        >
                                    {this.state.isLoggedUser?
                                    null
                                    :
                                    <Grid item >
                                        <TextField 
                                        id="logIn-name"
                                        inputRef={this.txtLogInMail} 
                                        fullWidth
                                        label="Correo" variant="outlined" />
                                    </Grid>
                                    }
                                    {this.state.isLoggedUser?
                                    null
                                    :
                                        <Grid item >
                                            <TextField 
                                            id="logIn-password"
                                            inputRef={this.txtLogInPassword} 
                                            fullWidth
                                            label="Password" type="password" variant="outlined" />
                                        </Grid>
                                    }
                                    {this.state.isLoggedUser?
                                        <Typography gutterBottom variant="body1">
                                            Nombre: {JSON.parse(localStorage.getItem('usr')).nombreUsuario + " " +JSON.parse(localStorage.getItem('usr')).apellidoUsuario}
                                        </Typography>
                                        
                                        
                                    :null}
                                            {/* <Grid item>
                                                <FormControlLabel
                                                    control={<Switch size="small" checked={this.state.switch_registrar} onChange={()=>{this.setState({switch_registrar:!this.state.switch_registrar})}} />}
                                                    label="Quiero crear mi cuenta"
                                                />
                                            </Grid> */}
                                            <Grid item >
                                                <Button 
                                                id={'btn_log_in_entrar_salir'}
                                                onClick={this.func_logIn}
                                                fullWidth
                                                variant="contained" color="primary">
                                                    {this.state.isLoggedUser?"Salir":"Entrar"}
                                                </Button>
                                            </Grid>
                                            {this.state.isLoggedUser?
                                            null
                                            :
                                            <Grid item >
                                                {/* <Button 
                                                onClick={this.func_logIn}
                                                fullWidth
                                                variant="contained" color="primary">
                                                    Registrarme
                                                </Button> */}
                                                <Register  />
                                            </Grid>
                                            

                                            }
                                            
                                            <Grid item >
                                                {/* <Button 
                                                onClick={this.func_logIn}
                                                fullWidth
                                                variant="contained" color="primary">
                                                    Registrarme
                                                </Button> */}
                                                <RegistroRestaurante  />
                                            </Grid>
                                            

                                            
                                        </Grid>
                                    </form>
                                </TabPanel> 
                        </div>
                </div>
            </SwipeableDrawer>
                <SwipeableDrawer
                    anchor={'right'}
                    open={this.state.toggle_shopping_cart}
                    onClose={this.toggleDrawerShoppginCart(false)}
                    onOpen={this.toggleDrawerShoppginCart(true)}
                    >
                    <div
                        role="presentation"
                        onClick={()=>{}}
                        onKeyDown={()=>{}}
                        >
                        <div>
                            <IconButton 
                                onClick={this.toggleDrawerShoppginCart()}
                                aria-label="Atras">
                                <ArrowBackIcon/>
                            </IconButton>
                            <Tabs
                                value={this.state.tabIndex}
                                onChange={this.tabOnChange}
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="Carrito"
                                centered={true}
                                >
                                <Tab 
                                
                                icon={<ShoppingCartIcon />}
                                label={"Carrito"}
                                />
                            </Tabs>
                            <TabPanel value={this.state.shoppingCarTabPanelIndex} index={0}>
                                {items}
                                <Typography variant="h6">
                                    Total: {this.state.shoppingCarTotal} 
                                </Typography>
                                <Button
                                disabled={
                                    (this.state.shoppingCarContent.length==0 || this.state.shoppingCarTotal<=0)

                                 }
                                onClick={()=>{
                                    this.displayNotification('success',"Vamos a comprar!")
                                    this.state.completedSteps[0]=true;
                                    this.state.activeStep=1
                                    this.setState({
                                        tabIndexRight:1,
                                        completedSteps:1,
                                        completedSteps:this.state.shoppingCarContent
                                    })
                                }}
                                fullWidth variant="contained" color="primary" component="span" endIcon={<ForwardIcon />}>
                                    Concretar envio
                                </Button>
                            </TabPanel> 
                        </div>
                    </div>
                </SwipeableDrawer>
            </Grid>
        </div>
        )
    }
}


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  titleBarTop: {
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.4) 0%, ' +
      'rgba(0,0,0,0.2) 70%, rgba(0,0,0,0) 100%)',
  },
  iconTop: {
    color: '#ffffff',
  },

}));
const useStyleNotifcation = makeStyles(theme=>{
    root:{
        top: theme.spacing(10)
    }
})

function TitlebarGridList(props) {
    const classes = useStyles();
    const [producDialogInfoOpen, setOpenProducDialogInfoOpen] = React.useState(false);
    const [productInfoCardContent, setOpenProductInfoCardContent] = React.useState(null);

    // const Transition = React.forwardRef(function Transition(props, ref) {
    //   return <Slide timeout={50} direction="down" ref={ref} {...props} />;
    // });


    const handleClickOpenProductdialogInfo = (val) => {
        console.log("VAL    ")
        console.log(val)
        axios.get(BASE_URL+':4010/py1/restaurante/'+val.Restaurante_idRestaurante).then(
            res=>{
                console.log(res)
                console.log(res.data.payload[0])
                const infoRes = res.data.payload[0];
                setOpenProducDialogInfoOpen(true);
                console.log(val)
                var dialogContent = <Card>
                    <CardMedia
                        style={{height:140}}
                        image={img_BASE_URL_S3+val.img}
                        title={val.name}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {val.name}
                        </Typography>
                        <Grid
                        container
                        spacing={2}
                        >
                            <Grid item>
                                <Typography gutterBottom variant="subtitle2">
                                    De: {infoRes.nombreRestaurante}
                                </Typography>

                            </Grid>
                            <Grid item>
                                <img style={{maxWidth:'30px',height:'auto'}} src={img_BASE_URL_S3+infoRes.logo}/>
                            </Grid>

                        </Grid>
                        <Typography variant="body2" color="textSecondary" component="p">
                            
                            {val.offer==0? 
                                "Precio: "+val.price+" Q"
                            :
                            <div>
                            <p>
                                {"Oferta: "+val.offer+" Q"}
                            </p>
                            <p>
                                <strike>{"Precio normal: "+val.price+" Q "}</strike>
                            </p>
                            </div>
                            }
                        </Typography>
                    </CardContent>
                </Card>;
                setOpenProductInfoCardContent(dialogContent)
            }
        )
    };
  
    const handleCloseProductdialogInfo = () => {
        setOpenProducDialogInfoOpen(false);

        setOpenProductInfoCardContent(null)
    };
  

    return (
        <div>
        <Grid
            container spacing={2}
            direction="row"
            justify="space-around"
            alignItems="center"
        >
            <Dialog
                open={producDialogInfoOpen}
                // TransitionComponent={Transition}
                keepMounted
                onClose={handleCloseProductdialogInfo}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                {productInfoCardContent}
            </Dialog>
            <Grid 
            item xs={12} md={12}
            >
                <Card>
                    <CardContent>
                        <Grid
                        container spacing={3}
                        direction="row"
                        justify="space-between"
                        alignItems="stretch"

                        >
                            <Grid 
                            item xs={8} sm={10} md={10} lg={11}
                            >

                                <TextField
                                    id="outlined-full-width"
                                    label="Buscar"
                                    // inputRef={this.txtBuscarProducto}
                                    style={{ marginLeft: 8,marginTop:8 }}
                                    placeholder={props.suggestion}
                                    helperText="De que tienes ganas hoy?"
                                    fullWidth
                                    margin="normal"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={props.handleChangeTxtBuscar}
                                    variant="filled"
                                />
                            </Grid>
                            {/* <Grid item xs={1} md={1}>
                                <IconButton color="primary" aria-label="more filters" component="span" 
                                onClick={props.onChangeMoreFilters}
                                >
                                    <FilterListIcon />
                                </IconButton>
                            </Grid> */}
                            <Grid item xs={4} sm={2} md={2} lg={1}>
                                <IconButton
                                // style={{"paddingLeft":"25px","paddingTop":"25px"}}
                                style={{"paddingTop":"25px"}}
                                color="primary" 
                                aria-label="more filters" 
                                component="span" 
                                onClick={props.onChangeProductShowOnlyOffers}
                                >
                                    <LoyaltyIcon />
                                <Typography 
                                variant="caption" 
                                align="center"
                                display="block" gutterBottom>
                                    {props.productShowOnlyOffers?
                                    "Ver todo"
                                    :
                                    "Ver ofertas calificacion"
                                    }
                                </Typography>
                                </IconButton>
                            </Grid>
                            {/* <Grid item xs={12} md={12}>
                                <Collapse in={props.showMoreFilters}>
                                    <Grid
                                        container spacing={2}
                                        direction="row"
                                        justify="flex-start"
                                        alignItems="center"
                                    >
                                     
                                        <Grid item xs={2}>
                                            Por precio:
                                        </Grid>
                                        <Grid item xs={5}>
                                            <TextField fullWidth type="number" id="standard-basic" label="Mayor que:" />
                                        </Grid>
                                        <Grid item xs={5}>
                                            <TextField fullWidth type="number" id="standard-basic" label="Menor que:" />
                                        </Grid> 
                                    </Grid>
                                </Collapse>
                            </Grid> */}
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

                
            <Grid 
            item xs={12}
            >
                <Hidden mdDown>
                    <GridList cols={3}  cellHeight={180} spacing={10} >
                    {props.tileData.map((val,index) => {
                        console.log(props.tileData);
                        console.log("AQUI")
                        var min = props.paginationIndex*props.paginationSize
                        var max = (props.paginationIndex+1)*props.paginationSize
                        if(index<min || index>=max){
                            return false
                        }
                        return(
                        <GridListTile key={val.id}>
                        <img src={img_BASE_URL_S3+val.img} alt={val.name} />
                        <GridListTileBar
                            titlePosition="top"
                            actionIcon={
                                <IconButton 
                                onClick={(e)=>{handleClickOpenProductdialogInfo(val)}}
                                aria-label={val.name} className={classes.iconTop}
                                >
                                    <InfoIcon />
                                </IconButton>
                            }
                            actionPosition="right"
                            className={classes.titleBarTop}
                        />
                        <GridListTileBar
                            title={val.name}
                            subtitle={<span>{val.offer!=0?"Oferta:"+ val.offer: "Precio:"+val.price} Q</span>}
                            actionIcon={
                            <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="center"                        
                            >
                                <Grid item xs={12}>
                                    <br/>
                                </Grid>
                                <Grid item xs={3}>
                                    <IconButton
                                    onClick={()=>{props.handleQuantityChangeOne(index,-1)}}
                                    aria-label={`Restar 1 ${val.name}`} className={classes.icon}>
                                        <RemoveCircleOutlineTwoToneIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField
                                    id={index+""}
                                    value={val.quantity}
                                    onChange={props.handleQuantityChange}
                                    inputProps={{min: 0, style: { textAlign: 'center', color:'#c1c4c4' }}} />
                                </Grid>
                                <Grid item xs={3}>
                                    <IconButton 
                                    onClick={()=>{props.handleQuantityChangeOne(index,1)}}                                
                                    aria-label={`Adicionar 1 ${val.name}`} className={classes.icon}>
                                        <AddCircleOutlineTwoToneIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={3}>
                                    <IconButton 
                                    onClick={()=>{
                                        props.displayNotification('success',"producto agregado al carrito")
                                        props.handleAddToCart(val.id,index)}}
                                    aria-label={`Agregar al carrito`} className={classes.icon}>
                                        <ShoppingCartTwoToneIcon />
                                    </IconButton>
                                </Grid>
                            </Grid>
                            }
                        />
                        </GridListTile>
                    )}
                    )}
                    </GridList>

                </Hidden>

                <Hidden lgUp>
                    <GridList cols={1}  cellHeight={180} spacing={10} >
                    {props.tileData.map((val,index) => {

                        var min = props.paginationIndex*props.paginationSize
                        var max = (props.paginationIndex+1)*props.paginationSize
                        if(index<min || index>=max){
                            return false
                        }
                        return(
                        <GridListTile key={val.id}>
                        <img src={img_BASE_URL_S3+val.img} alt={val.name} />
                        <GridListTileBar
                            titlePosition="top"
                            actionIcon={
                                <IconButton 
                                onClick={(e)=>{handleClickOpenProductdialogInfo(val)}}
                                aria-label={val.name} className={classes.iconTop}
                                >
                                    <InfoIcon />
                                </IconButton>
                            }
                            actionPosition="right"
                            className={classes.titleBarTop}
                        />
                        <GridListTileBar
                            title={val.name}
                            subtitle={<span>{val.offer!=0?"Oferta:"+ val.offer: "Precio:"+val.price} Q</span>}
                            actionIcon={
                            <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="center"   
                            >
                                <Grid item xs={12}>
                                    <br/>
                                </Grid>
                                <Grid item xs={2}>
                                    <IconButton
                                    onClick={()=>{props.handleQuantityChangeOne(index,-1)}}
                                    aria-label={`Restar 1 ${val.name}`} className={classes.icon}>
                                        <RemoveCircleOutlineTwoToneIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={2}>
                                    <TextField
                                    id={index+""}
                                    value={val.quantity}
                                    onChange={props.handleQuantityChange}
                                    inputProps={{min: 0, style: { textAlign: 'center', color:'#c1c4c4' }}} />
                                </Grid>
                                <Grid item xs={3}>
                                    <IconButton 
                                    onClick={()=>{props.handleQuantityChangeOne(index,1)}}                                
                                    aria-label={`Adicionar 1 ${val.name}`} className={classes.icon}>
                                        <AddCircleOutlineTwoToneIcon />
                                    </IconButton>
                                </Grid>
                                <Grid item xs={3}>
                                    <IconButton 
                                    onClick={()=>{
                                        props.displayNotification('success',"producto agregado al carrito")
                                        props.handleAddToCart(val.id,index)}}
                                    aria-label={`Agregar al carrito`} className={classes.icon}>
                                        <ShoppingCartTwoToneIcon />
                                    </IconButton>
                                </Grid>
                            </Grid>
                            }
                        />
                        </GridListTile>
                    )}
                    )}
                    </GridList>

                </Hidden>

            </Grid>
            <Grid 
            item xs={12}
            >
                <Grid 
                    container
                    direction="column"
                    justify="space-between"
                    alignItems="center"
                >
                    <Grid 
                    item xs={12}
                    >
                        <Pagination
                        onChange={props.handleChangePagination}
                        count={props.paginationLenght} 
                        color="primary" />
                    </Grid>
                </Grid>
                
                
            </Grid>

        </Grid>
      </div>
    );
  }



export default Dashboard
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Imort cssp
import PublishIcon from '@material-ui/icons/Publish';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Alert from "@material-ui/lab/Alert";
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Swal from "sweetalert2";
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import axios from 'axios';
const BASE_URL = 'http://35.222.169.75'

const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

  const Toast = Swal.mixin({
    toast: true,
    position: "top",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });
  

const DialogContent = withStyles((theme) => ({
root: {
    padding: theme.spacing(2),
},
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
root: {
    margin: 0,
    padding: theme.spacing(1),
},
}))(MuiDialogActions);


var urlimagen="";
var archivos;
class RegistroRestaurante extends React.Component{
    constructor(props){
        super(props)
        this.state={
            open:false,
            error:false,
            mensaje:false,
            alerta:false,
            vacio:false,
            imgB64:'',
            imgExtension:'',
            image:'',
        }

      
        // All refs
        this.txtName = React.createRef()
        this.txtApellido = React.createRef()
        this.txtPassword = React.createRef()
        this.txtAddress = React.createRef()
        this.txtEmail = React.createRef()
        // Eventos
        this.uploadImageOnChange = this.uploadImageOnChange.bind(this)
        this.handleClickOpen = this.handleClickOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.func_Registro = this.func_Registro.bind(this)
      this.func_Registrorestaurante = this.func_Registrorestaurante.bind(this)
      this.getBase64FomFile = this.getBase64FomFile.bind(this)
      this.pFileReader=this.pFileReader.bind(this)
    }
    //------API REGISTRARME
       // Interact with APIS



       func_Registro(){
      


        if(this.txtName.current.value=="" || this.txtPassword.current.value=="" || this.txtAddress.current.value=="" || this.txtApellido.current.value=="" || this.txtEmail.current.value==""   ){
          
         this.setState({vacio:true})
         setTimeout(() => {
            this.setState({vacio:false})
            
         }, 3000); 

           return;
          }
        // console.log(this.txtLogInMail.current.value )
        // console.log(this.txtLogInPassword.current.value)

        var user = {
            name: this.txtName.current.value,
            last_name: this.txtApellido.current.value,
            password: this.txtPassword.current.value,
            address:this.txtAddress.current.value,
            mail: this.txtEmail.current.value,
            idAvatar: this.txtApellido.current.value+"_"+ this.txtApellido.current.value,
            type : 1
            
        };
      
      
        axios.post(BASE_URL+':4000/py1/user/', user)
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.data.ok){
                console.log("SE REGISTRO CORRECTAMENTE")
                this.txtName.current.value="";
                this.txtApellido.current.value="";
                this.txtAddress.current.value="";
                this.txtEmail.current.value="";
                this.txtPassword.current.value="";
                this.setState({mensaje:true})
         setTimeout(() => {
            this.setState({mensaje:false})
            this.handleClose();
         }, 2000); 

         
            }else if(res.data.status==409){
                console.log("YA EXISTE UN USUARIO CON ESE CORREO")
                this.setState({alerta:true})
                setTimeout(() => {
                   this.setState({alerta:false})
                 
                }, 2000); 
                 
               // alert("Ya existe un usuario con ese correo registrado")
                this.txtEmail.current.value="";
            }else if(res.data.status==500){
                this.setState({error:true})
         setTimeout(() => {
            this.setState({error:false})
            this.handleClose();
         }, 2500); 
               
               
            }
           

        })
    }

    getBase64FomFile(img, callback){
        let fileReader = new FileReader();
        fileReader.addEventListener('load' ,function(evt){
          callback(fileReader.result);
        });
        fileReader.readAsDataURL(img);
      }

      
 pFileReader(file) {
    return new Promise((resolve, reject) => {
      var fr = new FileReader();
      fr.onload = () => {
        resolve(fr.result);
      };
      fr.readAsDataURL(file);
    });
  }
  
    func_Registrorestaurante(){

        
        if(this.txtName.current.value==""  || this.txtAddress.current.value==""  ){
          
            this.setState({vacio:true})
            setTimeout(() => {
               this.setState({vacio:false})
               
            }, 3000); 
   
              return;
             }
        var imgBase64 = this.state.imgB64
        var startOfB64 = imgBase64.indexOf(',')
        imgBase64 = imgBase64.substr(startOfB64,imgBase64.length)
        var cadena=imgBase64.split(',')
        const extension = this.state.imgExtension
        console.log('EL BASE 64 ES '+cadena[1])

        var restaurante = {
            name:this.txtName.current.value,
            direccion:this.txtAddress.current.value,
            id_usuario:1,//---mando uno quemado
            extension:extension,
            payload: imgBase64

        }

        axios.post(BASE_URL+':4010/py1/restaurante/', restaurante)
        .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.data.ok){
                console.log("SE REGISTRO CORRECTAMENTE EL RESTAURANTE")
                this.txtName.current.value="";
                this.txtAddress.current.value="";
             
                this.setState({mensaje:true})
         setTimeout(() => {
            this.setState({mensaje:false})
            this.handleClose();
         }, 2000); 

         
            }else if(res.data.status==409){
                console.log("YA EXISTE UN RESTAURANTE CON ESE NOMBRE")
                this.setState({alerta:true})
                setTimeout(() => {
                   this.setState({alerta:false})
                 
                }, 2000); 
                 
               // alert("Ya existe un usuario con ese correo registrado")
             
            }else if(res.data.status==500){
                this.setState({error:true})
         setTimeout(() => {
            this.setState({error:false})
            this.handleClose();
         }, 2500); 
               
               
            }
           

        })
       
    }

    uploadImageOnChange(event){
        if(this.state.haveOneRowSelected){
            this.setState({
                editProductImageChanged:true
            })
            // console.log('Editando pero cambio de imagen')
            // var img = event.target
            // console.log(img)
            // var canvas = document.createElement("canvas");
            // canvas.width = img.width;
            // canvas.height = img.height;
            // var ctx = canvas.getContext("2d");
            // ctx.drawImage(img, 0, 0);
            // var dataURL = canvas.toDataURL("image/png");
            
            // that.state.imgB64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            // //Extension
            // var lastIndexOfDot = event.target.src.lastIndexOf('.')

            // this.state.imgExtension = fileName.substr(lastIndexOfDot,fileName.length)
        }

        var fileName = event.target.files[0].name
        var lastIndexOfDot = fileName.lastIndexOf('.')

        this.state.imgExtension = fileName.substr(lastIndexOfDot,fileName.length)
        console.log(this.state.imgExtension)
        this.setState({image:URL.createObjectURL(event.target.files[0])})
        var FR= new FileReader();
        var that = this
        FR.addEventListener("load", function(e) {
            that.state.imgB64 = e.target.result
        })
        FR.readAsDataURL(event.target.files[0]);
    

    }
    handleClickOpen(){
        this.setState({open:true});
    };
    handleClose(){
        this.setState({open:false});
    };

    
    render(){

        return (



            <div>
                <Button  fullWidth
                                                variant="contained" color="primary" onClick={this.handleClickOpen}>
                    Registro Restaurante
                </Button>
                <Dialog 
                fullWidth={true}
                onClose={this.handleClose} aria-labelledby="customized-dialog-title" 
                open={this.state.open}>
                    <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
                        Registro Restaurante
                    </DialogTitle>
                    <DialogContent dividers>
                    <Grid
                            container 
                            spacing={3}
                            direction="column"
                            justify="center"
                            alignItems="stretch"
                        >
                    {this.state.error && (
              <Alert variant="filled" severity="error">
               Ocurrio un error en el servidor
              </Alert>
            )}

{this.state.mensaje && (
   
              <Alert variant="filled" severity="success">
               Restaurante creado correctamente
              </Alert>
                
            )}
            
{this.state.alerta && (
   
   <Alert variant="filled" severity="warning">
    Ya existe un restaurante registrado con ese nombre
   </Alert>
     
 )}

{this.state.vacio && (
   
   <Alert variant="filled" severity="warning">
    Necesita llenar todos los campos del formulario
   </Alert>
     
 )}
             </Grid>

             
             <br></br>
                        <Grid
                            container 
                            spacing={3}
                            direction="column"
                            justify="center"
                            alignItems="stretch"
                        >
                            <Grid item >
                                <TextField 
                                    id="txtName"
                                    inputRef={this.txtName} 
                                    fullWidth
                                    required
                                    label="Nombre" variant="outlined" />
                            </Grid>
                            <Grid item >
                                <TextField 
                                    id="txtApellido"
                                    inputRef={this.txtAddress} 
                                    required
                                    fullWidth
                                    label="Dirección" variant="outlined" />
                            </Grid>
                            <Grid item  xs={12} md={6}>
                                        <input
                                            accept="image/*"
                                            id="contained-button-file"
                                            multiple
                                            type="file"
                                            style={{display:'none'}}
                                            onChange={this.uploadImageOnChange}
                                        />
                                        <label htmlFor="contained-button-file">
                                            <Button fullWidth variant="contained" color="primary" component="span" endIcon={<PublishIcon />}>
                                                Seleccionar imagen
                                            </Button>
                                        </label>
                                    </Grid>
                                    <Grid item xs={6}>

                                        <Typography variant="subtitle2" >
                                            Imagen seleccioanda:
                                        </Typography>
                                        <br/>
                                        <img width="120px" height="auto" maxHeight="50px" src={this.state.image}/>
                                    </Grid>

                            <div className="flex justify-center py-4 lg:pt-4 pt-8  max-w-150-px">
<div  align= "center" className="flex justify-center"
                    id="preview"> </div>
                    </div>

                            
                        </Grid>
              
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button autoFocus onClick={this.func_Registrorestaurante} variant="contained" color="primary">
                            Registrar 
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
    
    );
    }
}

export default RegistroRestaurante
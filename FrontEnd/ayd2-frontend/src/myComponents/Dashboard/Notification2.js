import React from 'react'
import { makeStyles, Snackbar } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

const useStyle = makeStyles(theme=>{
    root:{
        top: theme.spacing(10)
    }
})
export default function Notification(props){
    const {notify, setNotify} = props
    const classes =  useStyle();
    const handleClose = (event, reason) => {
        if(reason === 'clickaway') return
        setNotify({
            ...notify,
            isOpen: false,
        })
    }
    return(
        <div>
            <Snackbar
                className={classes.root}
                open={props.isOpen}
                autoHideDuration={2500}
                anchorOrigin={{vertical:'top',horizontal:'right'}}
                onClose={props.handleClose}>
                <Alert severity={props.type}
                    onClose={props.handleClose}>
                    {props.message}
                </Alert>
            </Snackbar>
        </div>
    )
}
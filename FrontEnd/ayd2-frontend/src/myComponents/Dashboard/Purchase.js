import React from 'react';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import SendIcon from '@material-ui/icons/Send';
import Hidden from '@material-ui/core/Hidden';


import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import axios from 'axios';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { DatePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns'; // choose your lib
import MomentUtils from '@date-io/moment';
import moment from "moment";
import "moment/locale/es";

moment.locale("es");

var crypto = require("crypto");
const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
const IV_LENGTH = 16;


class Purchase extends React.Component{
    constructor(props){
        super(props)
        this.state={
            selectDirVal:-2,
            paraLlevar:false,
            pagarConTarjeta:false,
            selectAdrrArr:[],
            optionsInSelect:[]

        }
        // All refs
        this.txtName = React.createRef()
        this.txtAddress = React.createRef()

        this.txtNameCard = React.createRef()
        this.txtNumberCard = React.createRef()
        this.txtDeathDate = React.createRef()
        this.txtCorrelativve = React.createRef()
        
        // Methods
        this.selectDirHandleChange = this.selectDirHandleChange.bind(this)
        this.prepareIfUserIsLogged = this.prepareIfUserIsLogged.bind(this)
        this.decrypt = this.decrypt.bind(this)
        this.createOrder = this.createOrder.bind(this)
    }
    componentDidMount(){
        this.prepareIfUserIsLogged()
    }
    // Interaction with APIS
    createOrder(){
        // {
        //     "id_usuario": 3,
        //     "tipo_orden": false,
        //     "estado_orden": "1",
        //     "id_direccion": 2,
        //     "productos":[
        //           {
        //               "id_producto": 1,
        //               "cantidad":1,
        //               "precio": 35.50
        //           },
        //           {
        //               "id_producto": 2,
        //               "cantidad":1,
        //               "precio": 25.50
        //           }
        //       ]
        //   }
        // console.log(props.shoppingCarContentByRestaurant);
        var usrId = 1
        var actualUser=JSON.parse(localStorage.getItem('usr'))
        if (actualUser!=null){
            usrId = actualUser.idUsuario
        }
        var estado_orden = 0
        var id_direccion = ""
        if(this.state.selectDirVal==-2){
            id_direccion = null
        }

        var addressId = this.state.selectDirVal
        if(this.state.paraLlevar==false){
            addressId = 1
        }
        var orden = {
            id_usuario:usrId,
            tipo_orden:!this.state.paraLlevar,
            estado_orden:0,
            id_direccion:addressId,
            productos
        }

        console.log("CARRIT")
        console.log(this.props.shoppingCarContent)
        //obtener todos los id de los restaurantes que se utilizaron
        const id_restaurantes = Object.keys(this.props.shoppingCarContentByRestaurant);
        console.log('id_restaurantes')
        console.log(id_restaurantes)
        for(var id_rest in id_restaurantes){
            //recorrer todos los productos del carito y si alguno cuadra con este id vamos
            orden.id_restaurante = parseInt(id_restaurantes[id_rest])
            var productos = []
            this.props.shoppingCarContent.forEach(
                (element)=>{
                    console.log('id_rest')
                    console.log(id_rest)
                    console.log('element.Restaurante_idRestaurante')
                    console.log(element.Restaurante_idRestaurante)
                    if(element.Restaurante_idRestaurante==orden.id_restaurante){
                        var prod = {
                            id_producto:element.id,
                            cantidad:element.quantity,
                            precio:element.price
                        }
                        productos.push(prod)
                    }
                }
            )
            if(productos.length>0){
                orden.productos=productos;
                console.log("orden")
                console.log(orden)
                //Make the order
                axios.post(this.props.BASE_URL+':3010/orders/crear-orden', orden).then(res=>{
                    console.log(res)
                    console.log(res.data)
                    if(res.status==200){
                        this.props.displayNotification('success',"excelente!, orden en proceso")
                    }else{
                        this.props.displayNotification('error',"No se pudo procesar la orden :c")
                    }
                    console.log('Orde agregada')
                    this.props.nextStep()
                })

            }
        
        }
    // var productos_por_restaurante = {}
        // axios.post(this.props.BASE_URL+':3010/orders/crear-orden', orden).then(res=>{
        //     console.log(res)
        //     console.log(res.data)
        //     if(res.status==200){
        //         this.props.displayNotification('success',"excelente!, orden en proceso")
        //     }else{
        //         this.props.displayNotification('error',"No se pudo procesar la orden :c")
        //     }
        //     console.log('Orde agregada')
        //     this.props.nextStep()
        // })
    }
    // Utilities
    selectDirHandleChange(event){
        console.log(event.target.value)
        this.setState({selectDirVal:event.target.value})
    }
    prepareIfUserIsLogged(){
        var usr = JSON.parse(localStorage.getItem('usr'))
        console.log(usr)
        if (usr==null){
            return
        }
        this.txtName.current.value = usr.nombreUsuario + " " + usr.apellidoUsuario
        var encriptedAddress = usr.Direccion
        var i = 0
        
        encriptedAddress.forEach(addr=>{
            console.log('encript add')
            console.log(addr)
            console.log(addr.nombreDireccion)
            var add=this.decrypt(addr.nombreDireccion)
            console.log("decripted")
            console.log(add)
            this.state.selectAdrrArr.push({
                idDireccion:addr.idDireccion,
                text:add,
                idUsuario:addr.Usuario_idUsuario
            })
            this.state.optionsInSelect.push(
                <MenuItem key={addr.idDireccion} value={addr.idDireccion}>{add}</MenuItem>
            )
            if(this.state.selectDirVal==-2){
                this.state.selectDirVal=addr.idDireccion
            }
            i++
        })
        this.setState({
            selectAdrrArr:this.state.selectAdrrArr
        })
        
    }
    decrypt(text) {
        let textParts = text.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
      }
    render(){
        return(
            <div>
                <Card>
                    <CardContent>
                        <Grid
                            container
                            spacing={2}
                        >
                            <Hidden lgUp>
                                <Grid item xs={12} md={4}>
                                    <Card>
                                        <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Lista de compra
                                        </Typography>
                                            <Grid
                                                container spacing={3}
                                                direction="row"
                                                justify="space-around"
                                                alignItems="center"
                                            >
                                                <div style={{
                                                float:'left',
                                                width:'100%',
                                                overflowY: 'auto',

                                                }}>
                                                {this.props.items}
                                                </div>
                                                <Grid item xs={12}>
                                                    <Typography variant="h6">
                                                        Total: {this.props.shoppingCarTotal} 
                                                    </Typography>
                                                </Grid>

                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>

                            </Hidden>

                            <Grid item xs={12} md={8}>
                                <Card>
                                    <CardContent>
                                        <Grid
                                            container
                                            spacing={2}
                                        >
                                            <Grid item xs={12}>
                                                <Typography gutterBottom variant="h5" component="h2">
                                                    Concretar la compra
                                                </Typography>
                                            </Grid>
                                            {/* <Grid item xs={12}> */}
                                            <Grid
                                            item xs={12} md={12}
                                            >
                                                <TextField
                                                inputRef={this.txtName}
                                                fullWidth id="prod_name" label="Nombre" variant="outlined" />
                                            </Grid>
                                            <br/>
                                            <Grid item xs={12} md={6}>
                                                <FormControl variant="outlined" fullWidth >
                                                    <InputLabel id="demo-simple-select-outlined-label">Entrega</InputLabel>
                                                    <Select
                                                        labelId="demo-simple-select-outlined-label"
                                                        id="demo-simple-select-outlined"
                                                        value={this.state.paraLlevar?1:0}
                                                        onChange={()=>{this.setState({paraLlevar:!this.state.paraLlevar})}}
                                                        label="Metodo" >
                                                            <MenuItem value="0" selected>Recoger en tienda</MenuItem>
                                                            <MenuItem value="1" >A domicilio</MenuItem>
                                                    </Select>
                                                    
                                                </FormControl>
                                            </Grid>

                                            {this.state.paraLlevar?

                                                <Grid item md={6} xs={12}>
                                                    {localStorage.getItem("usr")==null?
                                                    <TextField 
                                                    inputRef={this.txtAddress}
                                                    fullWidth id="prod_name" label="Nueva direccion" variant="outlined" />

                                                    :
                                                    <FormControl variant="outlined" fullWidth >
                                                        <InputLabel id="demo-simple-select-outlined-label2">Dirección</InputLabel>
                                                        <Select
                                                        
                                                        labelId="demo-simple-select-outlined-labe2l"
                                                        id="demo-simple-select-outlined2"
                                                        value={this.state.selectDirVal}
                                                        onChange={this.selectDirHandleChange}
                                                        label="Direccion"
                                                        >
                                                            {/* <MenuItem value={-1}>Nueva</MenuItem> */}
                                                            {this.state.optionsInSelect}
                                                        </Select>
                                                    </FormControl> 
 
                                                    }
                                                </Grid>

                                            :
                                            null
                                            }
    
                                                <br/>
                                                <br/>

                                        
   
                                    {/* </Grid> */}

                                    </Grid>


                                    <Grid item xs={12}>
                                        <FormControlLabel
                                            control={
                                            <Switch
                                                checked={this.state.pagarConTarjeta}
                                                onChange={()=>{this.setState({pagarConTarjeta:!this.state.pagarConTarjeta})}}
                                                name="checkedB"
                                                color="primary"
                                            />
                                            }
                                            label={this.state.pagarConTarjeta?"Pagar con tarjeta":"En efectivo"}
                                        />
                                    </Grid>
                                    {this.state.pagarConTarjeta?
                                    <Grid
                                    container
                                    spacing={2}
                                    >
                                        <Grid
                                        item xs={12}>
                                            <TextField 
                                            inputRef={this.txtNameCard}
                                            fullWidth id="prod_name" label="Nombre" variant="outlined" />
                                        </Grid>
                                        <Grid
                                        item xs={12}>
                                            <TextField 
                                            inputRef={this.txtNumberCard}
                                            fullWidth id="txtNumCard" label="Numero" variant="outlined" />
                                        </Grid>

                                        <Grid
                                        item xs={6}>
                                            <MuiPickersUtilsProvider utils={MomentUtils} utils={MomentUtils} locale={moment.locale("es")}>
                                                <DatePicker
                                                    fullWidth
                                                    variant="inline"
                                                    inputVariant="outlined"
                                                    openTo="year"
                                                    views={["year", "month"]}
                                                    label="Fecha de vencimiento"
                                                    value={this.state.valueDatePicker}
                                                    onChange={(e)=>{this.setState({valueDatePicker:e})}}
                                                />

                                            </MuiPickersUtilsProvider>
                                        </Grid>
                                        <Grid
                                        item xs={6}>
                                            <TextField 
                                            inputRef={this.txtCorrelativve}
                                            fullWidth id="prod_name3" label="CVV" variant="outlined" />
                                        </Grid>
                                    </Grid>
                                    
                                    :null}
                                </CardContent>
                                    <CardActions>
                                        <Grid container spacing={2} 
                                            direction="row"
                                            justify="flex-end"
                                            alignItems="flex-end"
                                        >
                                            <Grid item >
                                                <Button 
                                                disabled={
                                                    (this.props.shoppingCarContent.length==0 || this.props.shoppingCarTotal<=0)
                                                }
                                                onClick={this.createOrder}
                                                color="primary" endIcon={<SendIcon />} variant="contained">Realizar pedido</Button>
                                            </Grid>
                                        </Grid>
                                    </CardActions>
                                </Card>
                            </Grid>
                            <Hidden mdDown>
                                <Grid item xs={12} md={4}>
                                    <Card>
                                        <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Lista de compra
                                        </Typography>
                                            <Grid
                                                container spacing={3}
                                                direction="row"
                                                justify="space-around"
                                                alignItems="center"
                                            >
                                                <div style={{
                                                float:'left',
                                                width:'100%',
                                                overflowY: 'auto',

                                                }}>
                                                {this.props.items}
                                                </div>
                                                <Grid item xs={12}>
                                                    <Typography variant="h6">
                                                        Total: {this.props.shoppingCarTotal} 
                                                    </Typography>
                                                </Grid>

                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>

                            </Hidden>
                        </Grid>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

export default Purchase
import React from 'react';
import { Dialog, DialogActions, DialogContent, DialogTitle, Typography, Button, makeStyles } from '@material-ui/core';

const useStyle = makeStyles(theme => ({
    dialog:{
        padding:theme.spacing(2),
        position:'absolute',
        top: theme.spacing(5)
    },
    dialogContent:{
        textAlign:'center'
    },
    dialogAction:{
        justifyContent:'center'
    }
}))

export default function ConfirmDialog(props){

    const {confirmDialog,setConfirmDialog} = props
    const classes = useStyle()

    const handleClose = (event,reason) => {
        setConfirmDialog({
            ...confirmDialog,
            isOpen:false
        })
    }

    return(
        <Dialog open={props.isOpen}>
            <DialogTitle>
                {/*add some icon*/}
            </DialogTitle>
            <DialogContent className={classes.dialogContent}>
                <Typography variant='h6'>
                    {props.title}
                </Typography>
                <Typography variant='subtitle2'>
                    {props.subtitle}
                </Typography>
            </DialogContent>
            <DialogActions className={classes.dialogAction}>
            <Button onClick={props.handleClose} color="primary">
                No
            </Button>
            <Button onClick={props.onConfirm} color="primary" autoFocus>
                Si
            </Button>
            </DialogActions>
        </Dialog> 
    )
}
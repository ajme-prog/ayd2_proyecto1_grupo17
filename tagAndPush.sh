#!/bin/bash
set -eo pipefail
echo "$CI_COMMIT_BRANCH"

echo "Subiendo las imagenes de los contenedores de prueba a DockerHub"

# Se agregan los tag a las imagenes, puede construirse nuevamente con... docker build -t
# docker build -t registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17 .

echo "Tagging"
docker tag frontend registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:frontend${VERSION}
docker tag ms-orden-eliminar registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-eliminar${VERSION}
docker tag ms-orden-obtener registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-obtener${VERSION}
docker tag ms-orden-actualizar registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-actualizar${VERSION}
docker tag servicio2 registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:servicio2${VERSION}
docker tag servicio1 registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:servicio1${VERSION}
docker tag ms-orden-crear registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-crear${VERSION}
docker tag mydb registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:mydb${VERSION}

echo "Pushing"
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:frontend${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-eliminar${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-obtener${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-actualizar${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:servicio2${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:servicio1${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:ms-orden-crear${VERSION}
docker push registry.gitlab.com/ajme-prog/ayd2_proyecto1_grupo17:mydb${VERSION}

echo "End of script"
require('dotenv').config();
const express = require('express');
const app = express();
const router = require('./routes/index');
const port = process.env.PORT;

const logger = require('morgan');
const cors = require('cors');

/* middlewares */
app.use(logger('dev'));
app.use(cors());
app.use(express.json({limit:'10mb'}));
app.use(express.urlencoded({extended: '10mb'}))

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.use('/py1', router);

app.listen( port || 4000, (err) => {
    console.log('Server Usuarios running on port ' + (port || 4000))
});
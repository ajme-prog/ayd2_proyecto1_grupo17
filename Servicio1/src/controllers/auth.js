var crypto = require("crypto");
const {sign} = require('jsonwebtoken')
const response = require("./response");
const { read_user_db } = require('../db_apis/auth')
const { insertErrorSistema,insertErrorUsuario1 } = require('./log-error');
const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
const IV_LENGTH = 16;

async function login(req, res){
    const mail = req.body.mail;
    const password= req.body.password;
 
    try{ 
        /*var mykey = crypto.createCipheriv('aes-128-cbc', 'd6F3Efeq');
        var mystr = mykey.update(password, 'utf8', 'hex')
        mystr += mykey.final('hex');*/
        //var mystr= decrypt(password)

        r = await read_user_db(mail);

        // check if the password is correct
        if(decrypt(r[0].password)==password){
            // Generate the token
            const token = generate_token({user_id:r[0].idUsuario})
            //send successful response
            response(res,200,true,{ 'id':r[0].idUsuario, token },'Se inicio sesion de forma exitosa');
        }else{
            await insertErrorUsuario1(['Error! La contraseña o el correo no son correctos',JSON.stringify(req.body).substring(0,99)]);
            response(res,409,false,'','La contraseña o el correo no son correctos');
        }
    
    }catch (error) {
        await insertErrorSistema([JSON.stringify(error)]);
        response(res,500,false,'','Error en el servidor al iniciar sesión: ' + error);
        console.log(error);
    }
}

function generate_token(payload){
    return sign( payload, 
        process.env.SECRET, 
        { expiresIn: '2h' });
}

  function decrypt(text) {
    let textParts = text.split(':');
    let iv = Buffer.from(textParts.shift(), 'hex');
    let encryptedText = Buffer.from(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
  }


module.exports = {
    login
}
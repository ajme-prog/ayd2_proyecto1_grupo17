const response = require("./response");
const {verify} = require('jsonwebtoken')

async function verifyToken(req, res, next){
    try {
        const token = req.headers.authorization.replace('Bearer ','')
        if(token){
            verify(token, process.env.SECRET, function(err, _) {
                if(err){
                     return response(res,500,false,'','Error el token no es correcto');
                } else return next();
            });
        } else return response(res,400,false,{},'Token no proveido');
    } catch (error) {
        return response(res,400,false,{},'Token no proveido');
    }
}

module.exports = verifyToken;
const db = require('../services/db')

async function insertErrorSistema(parameters){
    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,mensajeErrorSistema)VALUES(NOW(),'Sistema','Servicio1',?)`,[parameters]))
}
async function insertErrorUsuario1(para1,para2){
    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,descripcionError,entradaError) 
    VALUES(NOW(),?,?,?)`,['Usuario','Servicio1',para1 ,para2]))
}

module.exports = {
    insertErrorSistema,
    insertErrorUsuario1
};
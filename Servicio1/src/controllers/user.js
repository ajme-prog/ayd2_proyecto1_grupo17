var crypto = require("crypto");
const response = require("./response");
const { insertErrorSistema,insertErrorUsuario1 } = require('./log-error');
const { search_mail } = require('../db_apis/auth')
const { get_user_db, add_user_db, update_user_db, get_address_db,get_restaurante_db } = require("../db_apis/user");
const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
const IV_LENGTH = 16;

async function get_user(req, res) {
  try{
    let r = await get_user_db(req.params.id) // here call this function
    let d = await get_address_db(req.params.id)
    let idRes="";
    let nombreRestaurante="";
    let direccionRestaurante="";
    let logoRestaurante="";
    
    if(r[0].tipoUsuario==0){
      let j= await get_restaurante_db(r[0].Restaurante_idRestaurante);
      console.log(j)
      idRes=j[0].idRestaurante;
      nombreRestaurante= j[0].nombreRestaurante;
      direccionRestaurante= j[0].nombreDireccion;
      logoRestaurante= j[0].logo;
    }
    var json={
      idUsuario: r[0].idUsuario,
      nombreUsuario: r[0].nombreUsuario,
      apellidoUsuario: r[0].apellidoUsuario,
      correo: r[0].correo,
      idAvatar: r[0].idAvatar,
      tipoUsuario: r[0].tipoUsuario,
      Direccion: d,
      idRestaurante: idRes,
      nombreRestaurante: nombreRestaurante,
      direccionRestaurante:direccionRestaurante,
      logoRestaurante: logoRestaurante
    }
    /*var mykey2 = crypto.createDecipheriv('aes-128-cbc', 'd6F3Efeq');
    var mystr2 = mykey2.update(d., 'hex', 'utf8')
    mystr2 += mykey2.final('utf8');*/
    if(r == null)
    { 
      await insertErrorUsuario1(['Error! El Usuario no existe ',JSON.stringify(req.body).substring(0,99)]);
      return response(res,409,false,r,'El Usuario no existe');
    } else{
      response(res,200,true,json,"Datos del usuario");
    } 
  }catch (error) {
    await insertErrorSistema([JSON.stringify(error)]);
    console.log(error);
    response(res,500,false,null,'Error en el servidor al Registrar el usuario: ' + error);
  }
}

async function add_user(req, res) {
  const name = req.body.name;
  const lastName= req.body.last_name;
  const password= req.body.password;
  const address= req.body.address;
  const mail = req.body.mail;
  const idAvatar= req.body.id_avatar;
  const type = req.body.type;
  const id_restaurante = req.body.id_restaurante;
  
  try{ 
    // check if the user exist
    let r  = await search_mail(mail);
    //console.log(r);
    if(r != null)
    { 
      await insertErrorUsuario1(['Error! El correo ya se encuentra registrado ',JSON.stringify(req.body).substring(0,99)]);
      return response(res,409,false,r,'El correo ya se encuentra registrado');
    } else{

    } 
    /*var mykey = crypto.createCipheriv('aes-128-cbc', 'd6F3Efeq');
    var mystr = mykey.update(password, 'utf8', 'hex')
    mystr += mykey.final('hex');

    var mykey2 = crypto.createCipheriv('aes-128-cbc', 'd6F3Efeq');
    var mystr2 = mykey2.update(address, 'utf8', 'hex')
    mystr2 += mykey2.final('hex');*/

    var mystr= encrypt(password);
    var mystr2= encrypt(address);
    console.log(mystr)
    console.log(decrypt(mystr))
    // the user is added to the db
    r = await add_user_db(name, lastName,mystr,mail,mystr2,idAvatar,type,id_restaurante);
    response(res,200,true,r.insertId,"Se agrego el usuario de forma exitosa");
  }catch (error) {
    await insertErrorSistema([JSON.stringify(error)]);
    console.log(error);
    response(res,500,false,null,'Error en el servidor al Registrar el usuario: ' + error);
  }
}

async function update_user(req, res) {
  update_user_db() // here call this function
  response(res,200,true,{},"Crear la API para actualizar los datos del usuario"); 
}


function encrypt(text) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32),iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

module.exports = {
  get_user,
  add_user,
  update_user
}

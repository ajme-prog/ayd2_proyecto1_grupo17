const db = require('../services/db')

async function search_mail(mail){
    const r = await(db.query('select correo from Usuario where correo= ?;',[mail]))
    return r[0]? r[0] : null;
}

async function read_user_db(mail){
    return await(db.query('select idUsuario, password from Usuario where correo= ?;',[mail]))
}

module.exports = {
    search_mail,
    read_user_db
}
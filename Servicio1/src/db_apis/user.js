const db = require('../services/db')

async function get_user_db(user_id){
    return await(db.query('select * from Usuario where idUsuario= ?;',[user_id]))
}
async function get_address_db(user_id){
    return await(db.query('select * from DireccionDomiciliaria where Usuario_idUsuario= ?;',[user_id]))
}
async function get_restaurante_db(restaurante_id){
    return await(db.query('select idRestaurante,nombreRestaurante,logo,nombreDireccion from Restaurante inner join DireccionDomiciliaria on idDireccion=Restaurante.Direccion_idDireccion where idRestaurante= ?;',[restaurante_id]))
}

async function add_user_db(nombreUsuario, apellidoUsuario, password,correo,direccion,idAvatar,tipoUsuario,id_restaurante){
    return await(db.query('CALL insertUsuario(?,?,?,?,?,?,?,?,?,?);',[nombreUsuario,apellidoUsuario,password,correo,direccion,idAvatar,tipoUsuario,id_restaurante,'Servicio 1','Se registro un usuario']))
}


async function update_user_db(){
    return null
}

module.exports = {
    get_user_db,
    add_user_db,
    update_user_db,
    get_address_db,
    get_restaurante_db
}

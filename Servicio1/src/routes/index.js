const router = require('express').Router();
const users = require('./user')
const authentication = require('./auth')

router.use('',authentication)
router.use('/user',users)

module.exports = router;
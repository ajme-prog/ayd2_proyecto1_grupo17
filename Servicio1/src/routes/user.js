const router = require('express').Router()
const {get_user, add_user, update_user} = require('../controllers/user')
const verifyToken = require('../controllers/auth.middleware')

router.get('/:id',verifyToken,get_user)
router.post('',add_user)
router.post('/:id',verifyToken,update_user)

module.exports = router
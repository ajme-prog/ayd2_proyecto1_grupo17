const router = require('express').Router();
const { updateStateOrders } = require('../controllers/orders');

router.put('', updateStateOrders);                 // DEVUELVE TODAS LAS ORDENES QUE ALGUNAS VEZ SE HALLAN HECHO


module.exports = router; 
require('dotenv').config();
const express = require('express');
const app = express();
const router = require('./routes/index');

const logger = require('morgan');
const cors = require('cors');

/* middlewares */
app.use(logger('dev'));
app.use(cors());
app.use(express.json({limit:'10mb'}));
app.use(express.urlencoded({extended: '10mb'}))

const port = process.env.PORT;

app.get('/', (req, res) => {
    res.send('Hello World!')
});

// AQUI LLAMO EL ROUTER QUE TRAE TODO EL CODIGO DE LAS APIS
app.use('/orders', router);

app.listen( port || 5000, (err) => {
    console.log('Server Actualizar Orden running on port ' + (port || 3000))
});
const connection = require('../services/dbmysql');
const {nombreServidor} = require('../services/data');

async function update_State_OrdersDB(idOrden, idUsuario, estado) {
    let estadoNombre = '';
    switch(estado){
        case '0':{
            estadoNombre = 'Nueva Orden';
            break;
        }
        case '1':{
            estadoNombre = 'En Preparacion';
            break;
        }
        case '2':{
            estadoNombre = 'En camino';
            break;
        }
        case '3':{
            estadoNombre = 'Entregado';
            break;
        }
        case '4':{
            estadoNombre = 'Cancelada';
            break;
        }
        case '5':{
            estadoNombre = 'Pagada';
            break;
        }
    }
    const query = `CALL updateEstadoOrden(?,?,?,?,?)`;
    //const origen = 'Microservicio actualizar orden';
    const origen = nombreServidor.nombre;
    const descripcion = `La estado de la orden con el id ${idOrden} se actualizo a ${estadoNombre}`;
    
    await( connection.query(query,[idOrden, idUsuario, estado,origen,descripcion]) );
    return estadoNombre;
}


module.exports = {
    update_State_OrdersDB
};
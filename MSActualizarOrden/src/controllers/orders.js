const connection = require('../services/dbmysql');

const {
    insertErrorSistema,
    insertErrorUsuario
} = require('../services/log');

const {
    update_State_OrdersDB
} = require('../db_apis/orders')

async function updateStateOrders(req, res) {
    // Validate Request
    //console.log(req.body);
    const idOrden = parseInt( req.body.id_orden);
    const idUsuario = parseInt( req.body.id_usuario);
    const estado = req.body.estado;

    try {
        const rows = await update_State_OrdersDB(idOrden,idUsuario,estado);
        return res.status(200).json({ status:200, message:`Se cambio el estado de la orden a ${rows}` });
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        res.status(404);
        return res.json({'status': 404 ,'message': 'Ocurrio un error a la hora de actulizar el estado de la orden, por favor revisar peticion'})
    }
}

module.exports = {
    updateStateOrders
};
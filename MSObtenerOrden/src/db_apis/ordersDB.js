const connection = require('../services/dbmysql')

async function get_all_ordersDB() {
    //const query = 'SELECT * FROM Orden ORDER BY fecha DESC';
    const queryPrincipal = `SELECT Ord.idOrden AS id_orden, Ord.fecha, Ord.estadoOrden AS estado_orden, Ord.tipoOrden AS tipo_orden,
                        dd.nombreDireccion AS direccion, Us.nombreUsuario AS nombre_usuario, Us.apellidoUsuario AS apellido_usuario,
                        re.nombreRestaurante AS nombre_restaurante
                        FROM Orden as Ord
                        INNER JOIN DireccionDomiciliaria dd
                            ON Ord.Direccion_idDireccion = dd.idDireccion
                        INNER JOIN Usuario Us
                            ON Ord.Usuario_idUsuario = Us.idUsuario
                        INNER JOIN Restaurante re
                            ON Ord.Restaurante_idRestaurante = re.idRestaurante
                        ORDER BY Ord.fecha DESC`;

    let ordenes =  await( connection.query(queryPrincipal) );
    let result = await Promise.all( ordenes.map(async function(ord){
        //let keys = Object.keys(ord);
        //console.log(keys);
        //console.log(ord[keys[0]]);
        let orden = {
            id_orden: ord.id_orden,
            fecha: ord.fecha,
            estado_orden: ord.estado_orden,
            tipo_orden: ord.tipo_orden,
            direccion: ord.direccion,
            nombre_usuario: ord.nombre_usuario,
            apellido_usuario: ord.apellido_usuario,
            restaurante: ord.nombre_restaurante,
            productos: await( get_products_by_orders(ord.id_orden) )
        }
        //console.log(orden);
        return  orden 
    }) );
    //console.log('======================================');
    //console.log(result);
                    
    return await( result );
}

// CONSULTADO POR SI LAS MOSCAS
/*
SELECT Ord.fecha, 
		CASE 
			WHEN Ord.estadoOrden = '0' THEN 'Nueva'
			WHEN Ord.estadoOrden = '1' THEN 'Preparacion'
			WHEN Ord.estadoOrden = '2' THEN 'Camino'
			WHEN Ord.estadoOrden = '3' THEN 'Entregado'
			WHEN Ord.estadoOrden = '4' THEN 'Cancelada' 
		END as estado,
        CASE 
			WHEN Ord.tipoOrden = FALSE THEN 'Domicilio'
            WHEN Ord.tipoOrden = TRUE THEN 'Local'
		END AS tipo, dd.nombreDireccion
FROM Orden as Ord
INNER JOIN DireccionDomiciliaria dd
WHERE Ord.Direccion_idDireccion = dd.idDireccion AND Ord.Usuario_idUsuario = 6
ORDER BY Ord.fecha DESC;
*/
async function get_orders_userDB(id) {
    //const query = `SELECT * FROM Orden WHERE Usuario_idUsuario = ? ORDER BY fecha DESC`;
    const query = `SELECT Ord.idOrden AS id_orden, Ord.fecha, Ord.estadoOrden AS estado_orden,
                Ord.tipoOrden AS tipo_orden, dd.nombreDireccion AS direccion,
                re.nombreRestaurante AS nombre_restaurante
                FROM Orden as Ord
                INNER JOIN DireccionDomiciliaria dd
                    ON Ord.Direccion_idDireccion = dd.idDireccion
                INNER JOIN Restaurante re
                    ON Ord.Restaurante_idRestaurante = re.idRestaurante
                WHERE Ord.Usuario_idUsuario = ?
                ORDER BY Ord.fecha DESC`;

    let ordenes =  await( connection.query(query,[id]) );
    let result = await Promise.all( ordenes.map(async function(ord){
        //let keys = Object.keys(ord);
        //console.log(keys);
        //console.log(ord[keys[0]]);
        let orden = {
            id_orden: ord.id_orden,
            fecha: ord.fecha,
            estado_orden: ord.estado_orden,
            tipo_orden: ord.tipo_orden,
            direccion: ord.direccion,
            restaurante: ord.nombre_restaurante,
            productos: await( get_products_by_orders(ord.id_orden) )
        }
        //console.log(orden);
        return  orden 
    }) );
    //console.log('======================================');
    //console.log(result);
                    
    return await( result );
}

async function get_orders_pendientDB() {
    const query = `SELECT Ord.idOrden AS id_orden, Ord.fecha, Ord.estadoOrden AS estado_orden, Ord.tipoOrden AS tipo_orden,
                dd.nombreDireccion AS direccion, Us.nombreUsuario AS nombre_usuario, Us.apellidoUsuario AS apellido_usuario,
                re.nombreRestaurante AS nombre_restaurante
                FROM Orden as Ord
                INNER JOIN DireccionDomiciliaria dd
                    ON Ord.Direccion_idDireccion = dd.idDireccion
                INNER JOIN Usuario Us
                    ON Ord.Usuario_idUsuario = Us.idUsuario
                INNER JOIN Restaurante re
                    ON Ord.Restaurante_idRestaurante = re.idRestaurante
                WHERE Ord.estadoOrden != '3' AND Ord.estadoOrden != '4' AND Ord.estadoOrden != '5'
                ORDER BY Ord.fecha ASC;`;
    let ordenes =  await( connection.query(query) );
    let result = await Promise.all( ordenes.map(async function(ord){
        let orden = {
            id_orden: ord.id_orden,
            fecha: ord.fecha,
            estado_orden: ord.estado_orden,
            tipo_orden: ord.tipo_orden,
            direccion: ord.direccion,
            nombre_usuario: ord.nombre_usuario,
            apellido_usuario: ord.apellido_usuario,
            restaurante: ord.nombre_restaurante,
            productos: await( get_products_by_orders(ord.id_orden) )
        }
        return  orden 
    }) );
                    
    return await( result );
}

async function get_order_by_restaurantDB(idRestaurant) {
    //const query = `SELECT * FROM Orden WHERE Usuario_idUsuario = ? ORDER BY fecha DESC`;
    const query = `SELECT Ord.idOrden AS id_orden, Ord.fecha, Ord.estadoOrden AS estado_orden,
            Ord.tipoOrden AS tipo_orden, us.nombreUsuario AS nombre_usuario,
            us.apellidoUsuario AS apellido_usuario, dd.nombreDireccion AS direccion
            FROM Orden as Ord
            INNER JOIN DireccionDomiciliaria dd
                ON Ord.Direccion_idDireccion = dd.idDireccion
            INNER JOIN Usuario us
                ON Ord.Usuario_idUsuario = us.idUsuario
            WHERE Ord.Restaurante_idRestaurante = ?
            ORDER BY Ord.fecha DESC`;

    let ordenes =  await( connection.query(query,[idRestaurant]) );
    let result = await Promise.all( ordenes.map(async function(ord){
        //let keys = Object.keys(ord);
        //console.log(keys);
        //console.log(ord[keys[0]]);
        let orden = {
            id_orden: ord.id_orden,
            fecha: ord.fecha,
            estado_orden: ord.estado_orden,
            tipo_orden: ord.tipo_orden,
            nombre_usuario: ord.nombre_usuario,
            apellido_usuario: ord.apellido_usuario,
            direccion: ord.direccion,
            productos: await( get_products_by_orders(ord.id_orden) )
        }
        return  orden 
    }) );
                    
    return await( result );
}



async function get_products_by_orders(idOrden) {
    const query = `SELECT pr.nombreProducto AS nombre_producto, pr.imagen, dop.cantidadProducto AS cant_producto, dop.precio 
                    FROM detalleOrdenProducto dop
                    INNER JOIN Producto pr
                    ON dop.Producto_idProducto = pr.idProducto
                    WHERE dop.Orden_idOrden = ? ;`;
    return await( connection.query(query, [idOrden]) );
}

module.exports = {
    get_all_ordersDB,
    get_orders_userDB,
    get_orders_pendientDB,
    get_order_by_restaurantDB
}
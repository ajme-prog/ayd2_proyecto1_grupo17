const router = require('express').Router();
const { getAllOrders, 
    getOrdersUser } = require('../controllers/orders');

router.get('',getAllOrders);                 // DEVUELVE TODAS LAS ORDENES QUE ALGUNAS VEZ SE HALLAN HECHO
router.get('/:id', getOrdersUser);             // DEVUELVE TODAS LAS ORDENES DE UN USUARIO


module.exports = router; 
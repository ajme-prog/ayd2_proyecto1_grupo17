const router = require('express').Router();
const { 
    getOrdersPendient } = require('../controllers/orders');

router.get('', getOrdersPendient); // DEVUELVE ORDENES PENDIENTES

module.exports = router; 
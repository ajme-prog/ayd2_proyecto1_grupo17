const router = require('express').Router();
const orders = require('./orders');
const orders2 = require('./orders2');
const restaurant = require('./orderRestaurant');

router.use('/obtener-ordenes',orders);
router.use('/ordenes-pendientes', orders2);
router.use('/ordenes-restaurante', restaurant);

module.exports = router;
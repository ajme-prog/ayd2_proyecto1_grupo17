const router = require('express').Router();
const { 
    getOrdersRestaurant } = require('../controllers/orders');

router.get('',getOrdersRestaurant);

module.exports = router; 
const connection = require('../services/dbmysql')
const {
    insertErrorSistema,
    insertErrorUsuario
} = require('../services/log');
const {
    get_all_ordersDB,
    get_orders_userDB,
    get_orders_pendientDB,
    get_order_by_restaurantDB
} = require("../db_apis/ordersDB");

const mensaje = 'Ocurrio un error al intentar obtener todas las ordenes, por favor revisar la peticion';

async function getAllOrders(req,res) {
    try {
        const rows = await get_all_ordersDB();
        res.status(200);
        return res.json(rows);
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        res.status(404);
        return res.json({'status': 404 ,'message': mensaje})
    }
    /*const consulta = 'SELECT * FROM Orden ORDER BY fecha DESC';
    connection.query(consulta, (err, rows)=> {
        if(err) {
            insertErrorSistema(connection, JSON.stringify(err) );
            res.status(404);
            return res.json({'message': mensaje})
        }

        res.status(200);
        return res.json(rows);
    }); */
}

async function getOrdersUser(req,res) {
    let id = req.params.id;

    try {
        const rows = await get_orders_userDB(id);
        res.status(200);
        return res.json(rows);
    } catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        res.status(404);
        return res.json({'status': 404 ,'message': mensaje})
    }
    /*connection.query(`SELECT * FROM Orden WHERE idOrden = ${id} ORDER BY fecha DESC`, (err, rows)=> {
        //if(err) throw err;
        if(err) {
            insertErrorSistema(connection, JSON.stringify(err) );
            res.status(404);
            return res.json({'message':mensaje})
        }

        res.status(200);
        return res.json(rows);
    });*/
}

async function getOrdersPendient(req, res) {
    try {
        
        const rows = await get_orders_pendientDB();
        res.status(200);
        return res.json(rows);
    }catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        res.status(404);
        return res.json({'status': 404 ,'message': mensaje})
    }
}

async function getOrdersRestaurant(req,res) {
    const regex = /^[0-9]*$/;
    const isNumber = regex.test(req.query.id_restaurante);

    if(!isNumber){
        insertErrorSistema(connection,['Error! se requiere un id de restaurante valido',JSON.stringify(req.query)]);
        return res.status(404).send({ status:404, message: mensaje })
    }

    let id = parseInt(req.query.id_restaurante);

    try {
        const rows = await get_order_by_restaurantDB(id);
        res.status(200);
        return res.json(rows);
    } catch(err) {
        console.log(err);
        insertErrorSistema(connection, JSON.stringify(err) );
        res.status(404);
        return res.json({'status': 404 ,'message': mensaje})
    }
}

module.exports = {
    getAllOrders,
    getOrdersUser,
    getOrdersPendient,
    getOrdersRestaurant
};
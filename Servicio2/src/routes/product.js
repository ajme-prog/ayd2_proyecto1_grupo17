const router = require('express').Router()
const {get_all,get_productos_res, add_product, edit_product, delete_product,} = require('../controllers/product')

router.get('',get_all)
router.get('/:id',get_productos_res)
router.post('',add_product)
router.post('/:id',edit_product)
router.delete('',delete_product)

module.exports = router
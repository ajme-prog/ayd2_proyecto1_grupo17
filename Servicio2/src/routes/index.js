const router = require('express').Router();
const products = require('./product')
const restaurante = require('./restaurante')

router.use('/product',products)
router.use('/restaurante',restaurante)
module.exports = router;
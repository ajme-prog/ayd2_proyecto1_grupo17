const router = require('express').Router()
const {get_all,
    add_restaurante,
    get_one_restaurante,
    edit_restaurante,
    delete_restaurante,} = require('../controllers/restaurante')

router.get('',get_all)
router.get('/:id',get_one_restaurante)
router.post('',add_restaurante)

module.exports = router
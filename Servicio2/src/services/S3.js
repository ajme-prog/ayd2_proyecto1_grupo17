const AWS = require('./AWS')

const bucket_name = 'productos-ayd2'

const s3 = new AWS.S3({apiVersion: '2006-03-01'})

async function upload_file(path, payload, extension){
  return new Promise((resolve, reject) => {
    buffer = new Buffer.from(payload,'base64')

    const params = {
      Bucket: bucket_name,
      Key: path, // File name you want to save as in S3
      Body: buffer,
      ACL:'public-read',
      ContentEncoding: 'base64', // required
      ContentType: `image/${extension}`
    };
    
    // Uploading files to the bucket
    s3.upload(params, function(err, data) {
        if (err) {
          console.log(err)
          reject(err)
        }
        console.log(`File uploaded successfully. ${data.Location}`)
        resolve(`File uploaded successfully. ${data.Location}`)
    });
  })
}

module.exports.upload_file = upload_file
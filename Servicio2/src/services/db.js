const mysql = require('mysql');

const pool = mysql.createPool({
   connectionLimit : 10,
   host            : process.env.DB_HOST,
   user            : process.env.DB_USER,
   password        : process.env.DB_PASSWORD,
   database        : process.env.DB_DATABASE
});

let db = {};

db.query = (sqlString, values = []) => {
  return new Promise((resolve, reject) => {
    pool.query(sqlString, values,function (error, results /*, fields*/) {
      if (error) return reject(error);
      return resolve(results)
    });
  });
}

module.exports = db;
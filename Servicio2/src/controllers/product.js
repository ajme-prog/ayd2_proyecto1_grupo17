const uuid4 = require("uuid4");

const response = require("./response");
const {
  get_all_products,
  get_productos_restaurante,
  add_product_db,
  update_product_db,
  update_product_db2,
  delete_product_db,
  insertErrorSistema,
  insertErrorUsuario,
  find_one_product
} = require("../db_apis/product");
const { upload_file } = require("../services/S3");

async function get_all(req, res){
    try {
        const r = await get_all_products();
        return response(res,200,true,r);
    } catch (error) {
        await insertErrorSistema(JSON.stringify(error));
        console.log(error)
        return response(res,500,false,'','Error en el servidor de base de datos, no pudo obtener la informacion: ' + error);
    }
}
async function get_productos_res(req,res){
  try {
    const r = await get_productos_restaurante(req.params.id);
    response(res,200,true,r);
} catch (error) {
    await insertErrorSistema(JSON.stringify(error));
    response(res,500,false,'','Error en el servidor de base de datos, no pudo obtener la informacion: ' + error);
    console.log(error)
}
}

async function add_product(req, res){
    const name = req.body.name
    const extension = req.body.extension
    const file_name = 'producto/' + uuid4() + name + '.' + extension
    //---verifico el precio
    if (typeof req.body.precio !== 'number') {
      
      valor={ "name": req.body.name,
      "precio": req.body.precio,
       "extension": req.body.extension,
        "id_usuario": req.body.id_usuario}
 
     await insertErrorUsuario('El precio del producto que desea modificar debe ser un valor númerico',JSON.stringify(valor));
     
     console.log('No es un número');
     response(res,500,true,null,'Error de usuario al agregar el producto');
     return;
    
   } 


    try {
        // check if the product exist
        let r  = await find_one_product(name);
        if(r != null) return response(res,409,false,r,'El producto que desea agregar ya existe');
        if (isNaN(req.body.precio)) {
          await insertErrorUsuario('El precio del producto que desea crear debe ser un valor númerico');
          
          console.log('No es un número');
          response(res,500,true,null,'Error de usuario al agregar el producto');
          return;
        }
        // the product is added to the db
        r = await add_product_db(name,req.body.precio,file_name,req.body.id_restaurante,req.body.user_id);

        // the image is saved on S3     
        await upload_file(file_name, req.body.payload, extension)
        
        // send successful response
        response(res,200,true,r.insertId,'Se agrego el producto de forma exitosa');
        
    } catch (error) {
        await insertErrorSistema(JSON.stringify(error));
        response(res,500,true,null,'Error en el servidor al agregar el producto: ' + error);
        console.log(error);
    }

}

async function edit_product(req, res) {
  //res.send('edite un producto');
  const name = req.body.name;
  const extension = req.body.extension;
  const file_name = "producto/" + uuid4() + name + "." + extension;
  const oferta=req.body.oferta;
  if (typeof req.body.precio !== 'number') {
      
     valor={ "name": req.body.name,
     "precio": req.body.precio,
      "extension": req.body.extension,
       "id_usuario": req.body.id_usuario}

    await insertErrorUsuario('El precio del producto que desea modificar debe ser un valor númerico',JSON.stringify(valor));
    
    console.log('No es un número');
    response(res,500,true,null,'Error de usuario al agregar el producto');
    return;
   
  } 
try {  
  // the product is update to the db
  if (req.body.payload === "") {
    console.log("ACTUALICE PERO SIN FOTO ");

 
    let r = await update_product_db2(
      req.params.id,
      name,
      req.body.precio,
      oferta,
      req.body.id_usuario,
      "Servicio 2",
      "Actualización de Producto"
    );
    response(res, 200, true, "","Se edito el producto de forma exitosa");
    return;
  } else {

console.log('ENTRE A ACTUALIZAR CON FOTO')
   
  console.log("LLEGANDO")

    let r = await update_product_db(
      req.params.id,
      name,
      req.body.precio,
      file_name,
      oferta,
      req.body.id_usuario,
      "Servicio 2",
      "Actualización de Producto"
    );
    
    // the image is saved on S3
    await upload_file(file_name, req.body.payload, extension);
    response(res, 200, true,"", "Se edito el producto de forma exitosa");
    return;
  }
} catch(error){
  await insertErrorSistema(JSON.stringify(error));
  response(res,500,true,null,'Error en el servidor al editar el producto: ' + error);
}
  
}

async function delete_product(req, res) {
//console.log("estoy en delete product ");
console.log('los datos son '+req.body.id_usuario);
  try {
  let r = await delete_product_db(
    req.body.idProducto,
    req.body.id_usuario,
    "Servicio 2",
    "Eliminar Producto"
  );
  
  res.send({status:200,message:"Ok"});
}catch(error){
  await insertErrorSistema(JSON.stringify(error));
 // res.send({status:500,message:"Ok"});
 res.status(500).send({ok:true, status:500, payload:"", message:'Error en el servidor al eliminar el producto: ' + error});
 //response(res,500,true,null,'Error en el servidor al eliminar el producto: ' + error);
}
}

module.exports = {
  get_all,
  add_product,
  edit_product,
  delete_product,
  get_productos_res
};

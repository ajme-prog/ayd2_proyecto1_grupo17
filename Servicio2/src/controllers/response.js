const response = (res, status, ok, payload, message = '') => {
    res.status(status).json({ok, status, payload, message});
}

module.exports = response;
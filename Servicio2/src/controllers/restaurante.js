const uuid4 = require("uuid4");
var crypto = require("crypto");
const response = require("./response");
const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = 'd6F3Efeq'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
const IV_LENGTH = 16;
const {
  get_all_restaurantes,
  get_un_restaurante,
  add_restaurante_db,
  find_one_restaurante,
  obtener_ultimo_iddireccion,
  add_direccion_db,
  insertErrorSistema,
  insertErrorUsuario
} = require("../db_apis/restaurante");
const { upload_file } = require("../services/S3");

async function get_all(req, res){
    try {
        const r = await get_all_restaurantes();
        response(res,200,true,r);
    } catch (error) {
        await insertErrorSistema(JSON.stringify(error));
        response(res,500,false,'','Error en el servidor de base de datos, no pudo obtener la informacion: ' + error);
        console.log(error)
    }
}

async function get_one_restaurante(req, res){
  try {
      const r = await get_un_restaurante(req.params.id);
      response(res,200,true,r);
  } catch (error) {
      await insertErrorSistema(JSON.stringify(error));
      response(res,500,false,'','Error en el servidor de base de datos, no pudo obtener la informacion: ' + error);
      console.log(error)
  }
}
async function add_restaurante(req, res){
  const name = req.body.name
  const extension = req.body.extension
  const file_name = 'producto/' + uuid4() + name + '.' + extension
  //---verifico el precio
  if (typeof req.body.precio == 'number') {
    
    valor={ "name": req.body.name,
     "extension": req.body.extension,
      }

   await insertErrorUsuario('El nombre del restaurante que desea registrar debe ser alfanúmerico',JSON.stringify(valor));
   
   console.log('No es un número');
   response(res,500,true,null,'Error de usuario al agregar el producto');
   return;
  
 } 


  try {
      // check if the product exist
      let r  = await find_one_restaurante(name);
      if(r != null) return response(res,409,false,r,'El restaurante que desea agregar ya existe');
    //--agrego la direccion
    let nombredireccion=encrypt(req.body.direccion);
       let a= await add_direccion_db(nombredireccion);

       let b= await obtener_ultimo_iddireccion();

       console.log("EL ULTIMO ID DE LA DIRECCION ES "+b[0].id)
        r = await add_restaurante_db(name,file_name,parseInt(b[0].id),req.body.id_usuario);

      // the image is saved on S3    
      console.log("voy agregar la imagen en S3 y el payload es "+req.body.payload) 
      await upload_file(file_name, req.body.payload, extension)
      
      // send successful response
      response(res,200,true,r.insertId,'Se agrego el restaurante de forma exitosa');
      //response(res,200,true,a.insertId,'Se agrego el restaurante de forma exitosa');
  } catch (error) {
      await insertErrorSistema(JSON.stringify(error));
      response(res,500,true,null,'Error en el servidor al agregar el restaurante: ' + error);
      console.log(error);
  }

}

//--cambiar su funcionamiento
/*async function add_restaurante(req, res){
    const name = req.body.name
    const extension = req.body.extension
    const file_name = 'producto/' + uuid4() + name + '.' + extension
    //---verifico el precio
    if (typeof req.body.precio !== 'number') {
      
      valor={ "name": req.body.name,
      "precio": req.body.precio,
       "extension": req.body.extension,
        "id_usuario": req.body.id_usuario}
 
     await insertErrorUsuario('El precio del producto que desea modificar debe ser un valor númerico',JSON.stringify(valor));
     
     console.log('No es un número');
     response(res,500,true,null,'Error de usuario al agregar el producto');
     return;
    
   } 


    try {
        // check if the product exist
        let r  = await find_one_product(name);
        if(r != null) return response(res,409,false,r,'El producto que desea agregar ya existe');
        if (isNaN(req.body.precio)) {
          await insertErrorUsuario('El precio del producto que desea crear debe ser un valor númerico');
          
          console.log('No es un número');
          response(res,500,true,null,'Error de usuario al agregar el producto');
          return;
        }
        // the product is added to the db
        r = await add_product_db(name,req.body.precio,file_name,req.body.id_restaurante,req.body.user_id);

        // the image is saved on S3     
        await upload_file(file_name, req.body.payload, extension)
        
        // send successful response
        response(res,200,true,r.insertId,'Se agrego el producto de forma exitosa');
        
    } catch (error) {
        await insertErrorSistema(JSON.stringify(error));
        response(res,500,true,null,'Error en el servidor al agregar el producto: ' + error);
        console.log(error);
    }

}
//---cambiar su funcionamiento
async function edit_restaurante(req, res) {
  //res.send('edite un producto');
  const name = req.body.name;
  const extension = req.body.extension;
  const file_name = "producto/" + uuid4() + name + "." + extension;
  if (typeof req.body.precio !== 'number') {
      
     valor={ "name": req.body.name,
     "precio": req.body.precio,
      "extension": req.body.extension,
       "id_usuario": req.body.id_usuario}

    await insertErrorUsuario('El precio del producto que desea modificar debe ser un valor númerico',JSON.stringify(valor));
    
    console.log('No es un número');
    response(res,500,true,null,'Error de usuario al agregar el producto');
    return;
   
  } 
try {  
  // the product is update to the db
  if (req.body.payload === "") {
    console.log("ACTUALICE PERO SIN FOTO ");

 
    let r = await update_product_db2(
      req.params.id,
      name,
      req.body.precio,
      req.body.id_usuario,
      "Servicio 2",
      "Actualización de Producto"
    );
    response(res, 200, true, "","Se edito el producto de forma exitosa");
    return;
  } else {

console.log('ENTRE A ACTUALIZAR CON FOTO')
   
  console.log("LLEGANDO")

    let r = await update_product_db(
      req.params.id,
      name,
      req.body.precio,
      file_name,
      req.body.id_usuario,
      "Servicio 2",
      "Actualización de Producto"
    );
    
    // the image is saved on S3
    await upload_file(file_name, req.body.payload, extension);
    response(res, 200, true,"", "Se edito el producto de forma exitosa");
     return;
  }
} catch(error){
  await insertErrorSistema(JSON.stringify(error));
  response(res,500,true,null,'Error en el servidor al editar el producto: ' + error);
}
  
}
//---cambiar su funcionamiento
async function delete_restaurante(req, res) {
//console.log("estoy en delete product ");
console.log('los datos son '+req.body.id_usuario);
  try {
  let r = await delete_product_db(
    req.body.idProducto,
    req.body.id_usuario,
    "Servicio 2",
    "Eliminar Producto"
  );
  
  res.send({status:200,message:"Ok"});
}catch(error){
  await insertErrorSistema(JSON.stringify(error));
  response(res,500,true,null,'Error en el servidor al eliminar el producto: ' + error);
}
}
*/

function encrypt(text) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv(algorithm, Buffer.concat([Buffer.from(ENCRYPTION_KEY), Buffer.alloc(32)], 32),iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

module.exports = {
  get_all,
  get_one_restaurante,
  add_restaurante
};

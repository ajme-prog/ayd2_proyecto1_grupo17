const db = require('../services/db')

async function get_all_products(){
    return await(db.query('SELECT * FROM Producto'))
}

//----tengo que cambiarlo para que me devuelva el nombre del restaurante
async function get_productos_restaurante(id_restaurante) {
    return await (db.query('SELECT P.idProducto,P.nombreProducto,P.precio,P.imagen,P.oferta,P.Restaurante_idRestaurante,R.nombreRestaurante from Producto P INNER JOIN Restaurante R on R.idRestaurante=P.Restaurante_idRestaurante where P.Restaurante_idRestaurante = ?',[id_restaurante]))
}

async function add_product_db(name, precio, imagen,id_restaurante, user_id){
    let r = await(db.query('SELECT insertProducto(?,?,?,?,?,?,?)',[name,precio,imagen,id_restaurante,user_id,'Servicio2','New producto added ' + name]))
    key = Object.keys(r[0])
    return { insertId:r[0][key[0]]}
}

async function find_one_product(name){
    const r = await(db.query('SELECT idProducto FROM Producto WHERE nombreProducto = ?',[name]))
    return r[0]? r[0] : null;
}


async function update_product_db(id,name, precio, imagen,oferta,id_usuario,origen_log,descripcion_log){
    console.log("ENTRE A UPDATE_PRODUCTO");
    const r = await(db.query('UPDATE  Producto SET oferta = ? WHERE idProducto = ?',[oferta,id]))
   return await(db.query('call updateProducto1 (?, ?, ?, ?, ?, ?,?)',[id,name,precio,imagen,id_usuario,origen_log,descripcion_log]));
    }

async function update_product_db2(id,name, precio,oferta,id_usuario,origen_log,descripcion_log){
    const r = await(db.query('UPDATE  Producto SET oferta = ? WHERE idProducto = ?',[oferta,id]))
    return await(db.query('call updateProducto2(?, ?, ?, ?, ?, ?)',[id,name,precio,id_usuario,origen_log,descripcion_log]));
    
 }

async function delete_product_db(id,id_usuario,origen_log,descripcion_log) {
    console.log("EL DESCRIPCION LOG ES "+descripcion_log)
    return await(db.query('call deleteProducto(?, ?, ?, ?)',[id,id_usuario,origen_log,descripcion_log]));
    
}

async function insertErrorSistema(mensaje) {


    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,mensajeErrorSistema) VALUES(NOW(),'Sistema','Servicio2',?)`,[mensaje]));
   
}

async function insertErrorUsuario(mensaje,bodyjson) {

    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,descripcionError,entradaError) VALUES(NOW(),'Usuario','Servicio2',?,?)`,[mensaje,bodyjson]));
  
}


module.exports = {
    get_all_products,
    get_productos_restaurante,
    find_one_product,
    update_product_db2,
    update_product_db,
    delete_product_db,
    add_product_db,
    insertErrorSistema,
    insertErrorUsuario
}
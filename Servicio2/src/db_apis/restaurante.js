const db = require('../services/db')

async function get_all_restaurantes(){
    return await(db.query('SELECT * FROM Restaurante'))
}

async function get_un_restaurante(id) {
    return await(db.query('SELECT * FROM Restaurante where idRestaurante = ?',[id]))
}

async function add_restaurante_db(name,imagen,id_direccion, user_id){
    return await(db.query('CALL insertRestaurante(?,?,?,?,?,?)',[name,imagen,id_direccion,user_id,'Servicio2','Nuevo restaurante agregado ' + name]))
    
}

async function add_direccion_db(nombre) {
    return await (db.query('INSERT INTO DireccionDomiciliaria (nombreDireccion) VALUES (?)',[nombre]))
}
async function obtener_ultimo_iddireccion(){
  return await(db.query('SELECT MAX(idDireccion) AS id FROM DireccionDomiciliaria'))
    
}

async function insertErrorSistema(mensaje) {


    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,mensajeErrorSistema) VALUES(NOW(),'Sistema','Servicio2',?)`,[mensaje]));
   
}

async function insertErrorUsuario(mensaje,bodyjson) {

    return await(db.query(`INSERT INTO Error(fecha,nivelError,origen,descripcionError,entradaError) VALUES(NOW(),'Usuario','Servicio2',?,?)`,[mensaje,bodyjson]));
  
}

async function find_one_restaurante(name,direccion){
    const r = await(db.query('SELECT idRestaurante FROM Restaurante WHERE nombreRestaurante = ?',[name]))
    return r[0]? r[0] : null;
}



module.exports = {
    get_all_restaurantes,
    get_un_restaurante,
    add_restaurante_db,
    find_one_restaurante,
    add_direccion_db,
    obtener_ultimo_iddireccion,
    insertErrorSistema,
    insertErrorUsuario
}
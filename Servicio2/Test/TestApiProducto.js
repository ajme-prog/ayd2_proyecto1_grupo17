let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;
chai.use(chaiHttp);
const url= 'http://localhost:4010/py1';


//----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener todos los restaurantes: ',()=>{
    it('Deberia obtener todos los restaurantes', (done) => {
    chai.request(url)
    .get('/restaurante')
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
   

   //----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener todos los productos: ',()=>{
    it('Deberia obtener todos los productos de la BD', (done) => {
    chai.request(url)
    .get('/product')
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });

   //-------------prueba obtener los productos de un restaurante
      //----prueba correcta get all restaurante PRUEBA EXITOSA
describe('Obtener los productos de un restaurante especifico: ',()=>{
    it('Deberia obtener todos los productos de un restaurante', (done) => {
    chai.request(url)
    .get('/product/1')
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
   
//-----------FALLIDA DELETE PRODUCTO
   describe('Eliminar un producto especifico: ',()=>{
    it('Deberia eliminar un producto', (done) => {
    chai.request(url)
    .delete('/product')
    .send({idProducto:"200"})
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
   
//-----------EXITOSA DELETE PRODUCTO
   describe('Eliminar un producto especifico prueba buena: ',()=>{
    it('Deberia eliminar un producto', (done) => {
    chai.request(url)
    .delete('/product')
    .send({idProducto:-1,id_usuario:2})
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
   
   
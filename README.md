# AYD2 - Proyecto G17

## Instalar docker
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-es

## Instalar docker-compose
https://docs.docker.com/compose/install/

## Instalacion de GitLab Runner
https://docs.gitlab.com/runner/install/
https://docs.gitlab.com/runner/install/linux-repository.html

## Registrar el runner 
https://docs.gitlab.com/runner/register/index.html  
En siguiente archivo se almacena la configuracion de los runner registrados en la maquina:
```bash
/etc/gitlab-runner/config.toml
```
## Get started with GitLab CI/CD
Aqui se encuentra el procedimiento y ejemplo sencillo para crear el archivo .gitlab-ci.yml
https://docs.gitlab.com/ee/ci/quick_start/index.html

## Workflows
Procesamiento secuencial o en paralelo de los jobs
https://docs.gitlab.com/ee/ci/migration/circleci.html
https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/

## Permisos
En el archivo .gitlab-ci.yml al ejectuar el job:  
```yaml
build-job:
  stage: build
  tags:
   - VM-A
  script:
    - echo "Building containers for test"
    - docker-compose up --build -d
```
Da un error de permisos, porque los commando se ejecutan utilizando el usuario gitlab-runner, al cual debe darse permiso para poder utilizar docker sin tener que poner el comando sudo, esto se hace con el siguiente comando desde el server que funciona como runner:
```bash
sudo usermod -aG docker gitlab-runner
```